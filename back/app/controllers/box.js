import {makeBox, wipe} from "../../scripts/box.js";
import formidable from "formidable";
import {existsSync} from "fs";
import sequelize from "sequelize";
import moment from "moment";

const {Op} = sequelize

export const controller = {
  /**
   * Box index API
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param log
   * @returns {Promise<void>}
   */
  async index({req, res, models, craft, log}) {
    models['Box'].findAll({
      where: {UserId: req.user.id},
      attributes: ['code', 'name', 'type', 'deadline'],
      raw: true,
    })
      .then(data => res.send(craft(true, data)))
      .catch(err => {
        log(err)
        res.send(craft(false, err, 'Can not get boxes!'))
      })
  },

  /**
   * Box add API
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param log
   * @returns {Promise<void>}
   */
  async add({req, res, models, craft, log}) {
    // Generate Box id and type
    const box = {id: '', type: 'java', file: ''}
    // Prepare formidable instance
    const form = formidable({keepExtensions: true, maxFileSize: 5242880})
    // Upload file event
    this.upload({req, res, craft, form, box}, 1)
    // Parse form data
    form.parse(req, async (err, fields) => {
      models['Box'].create({...box, ...fields, UserId: req.user.id, isPublic: fields['password'] === ''})
        .then(() => res.send(craft(true)))
        .catch(() => {
          makeBox(id, type).destroy().truncate()
          log(err)
          res.send(craft(false))
        })
    })
  },

  /**
   * Handle upload event for formidable
   *
   * @param req
   * @param res
   * @param {function} craft
   * @param {formidable} form
   * @param {object} box
   * @param {int} mode
   */
  async upload({req, res, craft, form, box}, mode) {
    // Handle file
    form.onPart = part => part.name === 'file' || part.name === 'type' || part.name === 'id'
      ? (() => {
        mode === 2 && part.name === 'file' && makeBox(box.id, box.type).reload()
        part
          .on('data', buffer => {
            try {
              if (part.name === 'id') box[part.name] = mode === 1
                ? `${req.user.id}-${buffer.toString()}`
                : buffer.toString()
              else if (part.name === 'type') box[part.name] = buffer.toString()
              else {
                box[part.name] = part.filename
                makeBox(box.id, box.type)
                  .craft(buffer, /\.zip$/.test(part.filename) ? 'zip' : 'txt')
              }
            } catch (err) {
              res.send(craft(false, {}, 'An error has occurred!'))
            }
          })
          .on('end', () => part.name === 'file' && makeBox(box.id, box.type).extract(1, false))
      })()
      : form.handlePart(part)
  },

  /**
   * Box delete API
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param log
   */
  remove({req, res, models, craft, log}) {
    // Find box record
    models['Box'].findOne({
      where: req.params,
      attributes: ['id', 'type'],
      raw: true,
      rejectOnEmpty: true,
    })
      .then(data => {
        console.log('ahihi')
        // Delete the box containers
        makeBox(data['id'], data['type']).destroy().truncate()
        // Delete box record
        models['Box'].destroy({where: req.params})
          .then(() => res.send(craft(true, {})))
          .catch(err => res.send(craft(false, err, 'Can not delete box!')))
      })
      .catch(err => {
        log(err)
        console.log('nana')
        res.send(craft(false, err, 'Can not get box!'))
      })
  },

  /**
   * Box get API
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param log
   */
  async get({req, res, models, craft, log}) {
    models['Box'].findOne({where: req.params, raw: true})
      .then(data => res.send(craft(true, data)))
      .catch(err => {
        log(err)
        res.send(craft(false, err, 'Can not get box!'))
      })
  },

  /**
   * Box testcase download API
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param {function} log
   */
  async download({req, res, models, craft, log}) {
    // Get owner info
    models['Box'].findOne({
      where: req.params,
      attributes: ['id', 'UserId']
    })
      .then(data => {
        // Check if this is the same owner of the Box
        if (data.get('UserId') !== req.user.id) throw new Error('Denied!')
        // Get the path of Box
        const path = makeBox(data.get('id'), '').getPath()
        // Send file
        res.download(`${path}/archive.${existsSync(`${path}/archive.zip`) ? 'zip' : 'txt'}`)
      })
      .catch(err => {
        log(err)
        res.send(craft(false, err, 'Can not download file!'))
      })
  },

  /**
   * Box update API
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param log
   */
  async update({req, res, models, craft, log}) {
    // Generate Box id and type
    const box = {id: '', type: 'java', file: ''}
    // Prepare formidable instance
    const form = formidable({keepExtensions: true, maxFileSize: 5242880})
    // Upload file event
    this.upload({req, res, craft, form, box}, 2)
    // Parse form data
    form.parse(req, async (err, fields) => {
      models['Box'].update(
        {...box, ...fields, UserId: req.user.id, isPublic: fields['password'] === ''},
        {where: {code: fields['code']}}
      )
        .then(() => res.send(craft(true)))
        .catch(() => {
          log(err)
          makeBox(id, type).destroy().truncate()
          res.send(craft(false))
        })
    })
  },

  /**
   * Search for box
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param {function} craft
   * @param {function} log
   * @returns {Promise<void>}
   */
  async search({req, res, models, craft, log}) {
    const key = `%${req.params.key}%`
    models['Box'].findAll({
      where: {
        [Op.or]: [
          {code: {[Op.like]: key}},
          {name: {[Op.like]: key}},
          {deadline: {[Op.like]: key}}
        ],
        [Op.and]: [
          {deadline: {[Op.gte]: moment().format('YYYY-MM-DD hh:mm')}}
        ],
      },
      attributes: ['code', 'name', 'isPublic', 'deadline', 'type'],
      include: [{
        model: models['User'],
        attributes: ['name']
      }],
      raw: true
    })
      .then(data => res.send(craft(true, data)))
      .catch(err => {
        log(err)
        res.send(craft(false, err, 'Can not search boxes!'))
      })
  },

  async access({req, res, models, craft, log, is}) {
    models['Box'].findOne({
      where: {
        code: req.body['code'],
        deadline: {[Op.gte]: moment().format('YYYY-MM-DD hh:mm')}
      },
      attributes: ['id', 'name', 'password', 'note', 'entry', 'type', 'isFile', 'stdin', 'deadline'],
      raw: true,
    })
      .then(data => {
        if (is('empty', data)) {
          res.send(craft(false, {empty: true}))
        } else if (data['password'] === req.body['password']) {
          delete data['password']
          data['user'] = req.user.id
          res.send(craft(true, data, ''))
        } else {
          res.send(craft(false, {}, 'Wrong password!'))
        }
      })
      .catch(err => {
        log(err)
        res.send(craft(false))
      })
  }
}

