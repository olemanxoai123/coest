import moment from "moment";

export const controller = {
  async issue({req, res, models, craft, randomStr, hash, tokenExpiresIn}) {
    /** Sign token */
    const token = randomStr(40)

    /** Create token in DB */
    const auth = await models['Auth'].create({
      token: hash(token),
      UserId: req.user['id'],
      expiresAt: moment().add(...tokenExpiresIn()).toDate()
    })

    /** Return token */
    res.json(craft(true, {access_token: `${auth['id']}|${token}`}));

    return token['token']
  },

  logout({req, res, models}) {
    /** Hard delete current access token */
    models['Auth'].destroy({where: {id: req.user['auth']}})

    /** Response */
    res.status(200).send('')
  },

  truncate({req, res, models}) {
    /** Hard delete all access token */
    models['Auth'].destroy({where: {UserId: req.user['id']}})

    /** Response */
    res.status(200).send('')
  }
}

