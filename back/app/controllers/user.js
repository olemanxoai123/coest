export const controller = {
  async index({req, res, models, craft}) {
    const user = await models['User'].findOne({
      where: {id: req.user['id']},
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'password']
      }
    })

    res.send(craft(true, user))
  },
}

