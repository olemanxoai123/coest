import {makeBox, wipe} from "../../scripts/box.js";

export const controller = {
  upload({payload, socket, is}) {
    is('empty', payload['buffer'])
      ? (async () => {
        await makeBox(payload['id'], payload['type']).extract(payload['phase'])
        socket.emit('upload:done')
      })()
      : (async () => {
        makeBox(payload['id'], payload['type']).craft(payload['buffer'], payload['ext'])
        socket.emit('upload:continue')
      })()
  },

  async clone({payload, socket}) {
    socket.emit(
      'clone:done',
      makeBox(payload['id'], '').clone(payload['user'])
    )
  },

  async build({payload, socket}) {
    makeBox(payload['id'], payload['type']).build(payload['entry'])
    socket.emit('build:done')
  },

  async exec({payload, socket, exclude}) {
    makeBox(payload['id'], payload['type']).exec(
      pack => socket.emit('exec:update', exclude(pack, ['filename'])),
      () => socket.emit('exec:done'),
      {stdin: payload['stdin'], isFile: payload['isFile']}
    )
  },

  async reset({payload, socket}) {
    makeBox(payload['id'], payload['type']).destroy().reset()
    socket.emit('reset:done')
  },

  async truncate({payload, socket}) {
    makeBox(payload['id'], payload['type']).destroy().truncate()
    socket.emit('truncate:done')
  },

  async disconnect({socket}) {
    console.log(socket.id)
    wipe(socket.id)
  }
}

