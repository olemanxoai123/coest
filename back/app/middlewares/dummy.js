export const middleware = {
  log({next}) {
    console.log('This is middleware')

    next();
  },

  config({next}) {
    console.log('This is middleware for socket')

    next();
  }
};
