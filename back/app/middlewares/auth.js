import {compareSync} from "bcrypt"


export const middleware = {
  async login({req, res, next, models, is, craft, only}) {
    /** Validate request */
    if (is('empty', req.body['email']) || is('empty', req.body['password'])) {
      res.json(craft(false, {}, 'Credential has some missing field'))

      return false
    }

    /** Get user */
    const user = await models['User'].findOne({where: {email: req.body['email']}})
    if (is('empty', user) || !compareSync(req.body['password'], user['password'])) {
      res.json(craft(false, {}, 'Email/password incorrect'))

      return false
    }

    /** Pass user */
    req.user = only(user.get(), ['id'])

    next()
    return true
  },

  async register({req, res, next, models, is, craft, only, hashBcrypt}) {
    /** Validate request */
    if (
      is('empty', req.body['email']) ||
      is('empty', req.body['password']) ||
      is('empty', req.body['id'])
    ) {
      res.json(craft(false, {}, 'Some required field is missing'))

      return false
    }

    /** Check data overlap */
    let user = await models['User'].findOne({where: {email: req.body['email'], id: req.body['id']}})
    if (!is('empty', user)) {
      res.json(craft(false, {}, user['id'] === req.body['id']
        ? 'ID already exist'
        : 'Email already exist'
      ))

      return false
    }

    /** Modify some field */
    req.body['password'] = hashBcrypt(req.body['password'])

    /** Create credential */
    user = await models['User'].create(req.body)

    /** Pass user */
    req.user = only(user.get(), ['id'])

    next()
    return true
  },
};
