export const middleware = {
  /**
   * Check requester ownership
   *
   * @param req
   * @param res
   * @param {Sequelize.models} models
   * @param log
   * @param {function} next
   */
  ownership({req, res, models, craft, log, next}) {
    models['Box'].findOne({
      where: req.params,
      attributes: ['UserId'],
      rejectOnEmpty: true,
      raw: true
    })
      .then(data => data['UserId'] === req.user.id ? next() : res.status(403).send(`Forbidden`))
      .catch(err => {
        log(err)
        res.send(craft(false, {}, 'Empty'))
      })
  },
};
