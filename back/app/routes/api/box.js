export const routes = {
  '/add': {
    handler: {
      post: ['add']
    },
  },
  '/update/:code': {
    handler: {
      post: ['ownership', 'update']
    },
  },
  '/access': {
    handler: {
      post: ['access']
    },
  },
  '/index': 'index',
  '/get/:code': ['get'],
  '/download/:code': ['ownership', 'download'],
  '/remove/:code': ['ownership', 'remove'],
  '/search/:key': ['search'],
}
