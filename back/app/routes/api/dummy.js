export const routes = {
  '/endpoint1': ['log', 'test'],
  '/endpoint2': 'handle',
  '/endpoint3': {
    handler: {
      post: ['config', 'handle'],
    },
    auth: false
  }
}
