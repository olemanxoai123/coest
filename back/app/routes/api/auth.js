export const routes = {
  /** Testing API */
  '/test': 'test',
  /** Sign in API */
  '/login': {
    handler: {
      post: ['login', 'issue']
    },
    auth: false
  },
  /** Sign up API */
  '/register': {
    handler: {
      post: ['register', 'issue']
    },
    auth: false
  },
  /** Sign out API */
  '/logout': 'logout',
  /** Session truncate API */
  '/truncate': 'truncate'
}
