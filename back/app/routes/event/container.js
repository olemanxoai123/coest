export const events = {
  /**
   * Phase 1 + 2
   */
  'upload': 'upload',
  'clone': 'clone',
  /**
   * Phase 3
   */
  'build': 'build',
  /**
   * Phase 4
   */
  'exec': 'exec',
  /**
   * Phase 5: Reset or Truncate
   */
  'reset': 'reset',
  'truncate': 'truncate',
  'disconnect': 'disconnect',
}
