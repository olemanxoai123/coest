/** Node.js */
import http from "http";
import {basename} from 'path';
/** Express */
import express from "express";
import bodyParser from "body-parser";
/** Socket.io */
import {Server} from "socket.io";
/** Sequelize */
import sequelize from 'sequelize';
/** CLI Tool */
import cli from "./../scripts/cli.js";
import _ from "lodash"
/** Moment */
import moment from "moment";
/** Other */
import glob from "glob";
import * as utils from "../scripts/utils.js";

const {Sequelize, Model, Op} = sequelize;
const {is, log, hash, only} = utils;


/**
 * Build application
 *
 * @param {object} config Config object
 * @returns {Promise<{app: Express, httpServer: Server}>}
 */
export async function build(config) {
  /** Init requirement */
  Builder.init(config);

  /** Build resource */
  const sq = await Builder.buildResource();
  /** Build Socket.io server */
  const io = await Builder.buildSocket();
  /** Build app */
  await Builder.buildApp(io, sq);

  /** Return app, httpServer */
  return Builder.deliver();
}

/**
 * Application builder
 *
 * @class Builder
 */
export class Builder {
  /** Config object */
  static config = null;
  /** Express app instance */
  static app = null;
  /** Node HTTP server instance */
  static httpServer = null;
  /** CLI instance */
  static tools = null;
  /** Authenticate middleware */
  static authenticate = null;

  /**
   * Init app requirement
   *
   * @memberOf Builder
   * @static
   *
   * @param {object} config Config object
   */
  static init(config) {
    this.config = config;
    /** Get tools */
    this.tools = cli(!config['server']['cli']);

    /** Create app mandatory instances */
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}))

    this.httpServer = http.createServer(this.app);
  }

  /**
   * Deliver product
   *
   * @static
   *
   * @returns {object}
   */
  static deliver() {
    return {
      app: this.app,
      httpServer: this.httpServer
    }
  }

  /**
   * Responsible for building database
   *
   * @memberOf Builder
   * @static
   *
   * @returns {Promise<Sequelize>}
   */
  static async buildResource() {
    try {
      this.tools.start('Pack resources');
      /** Create Sequelize instance */
      let sq = new Sequelize(this.config.sequelize);

      /** Test connection */
      await sq.authenticate()
      /** Build models */
      await this.buildModels(sq);
      /** Build associations */
      await this.buildAssociations(sq);
      /** Sync Sequelize instance to database */

      await sq.sync();
      // sq.models['User'].findOne({ where: { title: 'ahoho' } }).then(v => console.log(v.title))

      this.tools.stop('succeed');
      return sq;
    } catch (error) {
      this.error('Cannot connect to database:\n', error);
    }
  }

  /**
   * Responsible for building socket server
   *
   * @memberOf Builder
   * @static
   *
   * @returns {Promise<Server<<ListenEvents,EmitEvents,ServerSideEvents>>>}
   */
  static async buildSocket() {
    /** Build server */
    const io = new Server(this.httpServer, this.config.socket)

    /** Setup server here */
    io.engine.on("connection_error", (err) => log(
      `[${err.code}] ${err.message}: {\n` +
      `\tRequest: ${err.req},\n` +
      `\tContext: ${err.context}\n` +
      `}`
      )
    );

    return io;
  }

  /**
   * Responsible for building application logic, routes, events, ...
   *
   * @memberOf Builder
   * @static
   *
   * @param {Server<ListenEvents,EmitEvents,ServerSideEvents>} io Socket.io Server instance
   * @param {Sequelize} sq Sequelize instance
   * @returns {Promise<void>}
   */
  static async buildApp(io, sq) {
    try {
      /** Build auth*/
      this.tools.start('Scaffold authentication');
      await this.buildAuthentication(io, sq);
      this.tools.stop('succeed');

      /** Build routes */
      this.tools.start('Bind routes');
      await this.buildRoutes(sq);
      this.tools.stop('succeed');

      /** Build sockets */
      this.tools.start('Link events');
      await this.buildEvents(io, sq);
      this.tools.stop('succeed');
    } catch (error) {
      this.error('Cannot build app:\n', error);
    }
  }

  /**
   * Build models for Sequelize instance
   *
   * @memberOf Builder
   * @static
   *
   * @param {Sequelize} sq Sequelize instance
   */
  static async buildModels(sq) {
    try {
      /** Search all models folder */
      for (const path of glob.sync('./app/models/*.js')) {
        const {schema, options} = await import(path.replace('/app', ''));
        const model = this.capitalize(basename(path).replace('.js', ''));

        /** Create model */
        sq.define(model, schema(sequelize.DataTypes), options);
      }
    } catch (error) {
      this.error('Cannot create models:\n', error);
    }
  }

  /**
   * Build models associations
   *
   * @memberOf Builder
   * @static
   *
   * @param {Sequelize} sq Sequelize instance
   */
  static async buildAssociations(sq) {
    try {
      /** Loop all models folder */
      for (const model in sq.models) {
        const {associations} = await import(`./models/${model.toLowerCase()}.js`);

        for (const target of Object.keys(associations)) {
          is('object', associations[target]) && 'type' in associations[target]
            ? sq.models[model][associations[target]['type']](sq.models[this.capitalize(target)], associations[target]['options'])
            : sq.models[model][associations[target]](sq.models[this.capitalize(target)]);
        }
      }
    } catch (error) {
      this.error('Cannot link models:\n', error);
    }
  }

  /**
   * Build authentication controller, middleware, ...
   *
   * @memberOf Builder
   * @static
   *
   * @param {Server<ListenEvents,EmitEvents,ServerSideEvents>} io Socket.io Server instance
   * @param {Sequelize} sq Sequelize instance
   * @returns {Promise<void>}
   */
  static async buildAuthentication(io, sq) {
    /** Build auth middleware for API */
    this.authenticate = (req, res, next) => this.authLogic(
      sq,
      req.headers['authorization'] ?? req.headers['x_authorization'],
      (user) => req.user = user,
      () => next(),
      () => res.status(401).send('Unauthorized')
    )

    /** Build Token authenticate for Socket.io events */
    io.use((socket, next) => this.authLogic(
      sq,
      socket.request.headers['authorization'] ?? socket.request.query,
      (user) => socket.user = user,
      () => next(),
      () => next(new Error('Unauthorized'))
    ))
  }

  /**
   * Authentication logic wrapper
   *
   * @memberOf Builder
   * @static
   *
   * @param {Sequelize} sq
   * @param {string} token
   * @param {function} pass
   * @param {function} authorized
   * @param {function} unauthorized
   */
  static authLogic(sq, token, pass, authorized, unauthorized) {
    /** Unauthorized if Authorization header is empty */
    is('empty', token)
      ? unauthorized()
      : (() => {
        /** Detect if id is included with token */
        const segment = token.split('|')
        const isPlain = segment.length === 1

        /** Check for token existence in DB */
        sq.models['Auth'].findOne({
          where: Object.assign(
            isPlain
              ? {token: hash(segment[0])}
              : {id: segment[0]},
            {expiresAt: {[Op.gt]: moment().toDate()}}
          ),
          attributes: [['UserId', 'id'], ['id', 'auth'], 'token'],
          include: {
            model: sq.models['User'],
            attributes: ['role']
          },
          raw: true,
          rejectOnEmpty: true
        })
          .then((data) => {
            /** Pass authentication data to req */
            const user = data
            pass({...only(user, ['id', 'auth']), role: user['User.role']})

            /** Unauthorized if token is not exited */
            isPlain
              ? authorized() /** If this is a plain token, compare process is already done */
              : (
                data['token'] === hash(segment[1])
                  ? authorized() /** Continue if token and hashed token is the same */
                  : unauthorized() /** If not, return Unauthorized */
              )
          })
          .catch(unauthorized)
      })()
  }

  /**
   * Build all api routes
   *
   * @memberOf Builder
   * @static
   *
   * @param {Sequelize} sq
   * @returns {Promise<void>}
   */
  static async buildRoutes(sq) {
    try {
      for (const path of glob.sync('./app/routes/api/*.js')) {
        /** Load routes info */
        const {routes} = await import(path.replace('/app', ''));

        /** Bind logic to routes */
        await this.bindRoutes(
          routes,
          basename(path),
          sq.models
        );
      }
    } catch (error) {
      log('Cannot build routes:\n', error);
    }
  }

  /**
   * Build all socket events
   *
   * @memberOf Builder
   * @static
   *
   * @param {Server<ListenEvents,EmitEvents,ServerSideEvents>} io Socket.io Server instance
   * @param {Sequelize} sq Sequelize instance
   * @returns {Promise<void>}
   */
  static async buildEvents(io, sq) {
    io.on('connection', async (socket) => {
      socket.on('init', async (module) => {
        try {
          /** Load events */
          const {events} = await import(`./routes/event/${module}.js`);
          /** Get controller */
          const {controller} = await import(`./controllers/${module}.js`);

          /** Bind event and controller */
          for (const [e, h] of Object.entries(events)) {
            socket.on(e, (payload) => controller[h]({io, socket, payload, models: sq.models, ...utils}))
          }

          /** Notify for success connection */
          socket.emit('status', true);

          /** Manual disconnect from client */
          socket.on('drop', () => socket.disconnect(true))

          /** Disconnect */
          socket.on('disconnect', console.log)
        } catch (error) {
          console.error('Cannot build sockets:\n', error);
          /** Notify for fail connection */
          socket.emit('status', false);
          /** Disconnect client */
          socket.disconnect(true)
        }
      })
    })
  }

  /**
   * Bind logic to routes
   *
   * @memberOf Builder
   * @static
   *
   * @param {object} routes
   * @param {string} basename
   * @param {Model} models
   * @param {boolean} testing
   * @returns {Promise<[]>}
   */
  static async bindRoutes(routes, basename, models, testing= false) {
    try {
      /** Get middleware */
      const {middleware} = await (async () => {
        try {
          return await import(`./middlewares/${basename}`)
        } catch {
          return {middleware: {}}
        }
      })();

      /** Get controller */
      const {controller} = await import(`./controllers/${basename}`);

      /** For testing */
      const infos = []

      /** Iterate through all routes for binding */
      for (const path of Object.keys(routes)) {
        /** Create route's info */
        const info = {
          path: path,
          basename: basename.replace('.js', ''),
          def: routes[path],
          models,
          auth: is('empty', routes[path]['auth']),
          controller: controller,
          middleware: middleware
        }

        testing && infos.push(Object.assign({}, info))

        /** Treat info */
        is('string', routes[path])
          ? this.treatDefAsString(info)
          : (
            is('array', routes[path])
              ? this.treatDefAsArray(info)
              : this.treatDefAsObject(info)
          )
      }

      return infos
    } catch (error) {
      this.error(`Cannot import ${basename} for binding routes to app: \n`, error.stack);
    }
  }

  /**
   * Treat routes value as string
   *
   * @memberOf Builder
   * @static
   *
   * @param {object} info Route's info
   */
  static treatDefAsString(info) {
    this.app[this.getMethod(info['method'])](
      `${this.config.api.prefix.trim()}/${info['basename']}${info['path']}`,
      info['auth'] === true
        ? this.authenticate
        : (req, res, next) => next(),
      (req, res) => info['controller'][info['def']]({req, res, models: info['models'], ...utils})
    );
  }

  /**
   * Treat routes value as array
   *
   * @memberOf Builder
   * @static
   *
   * @param {object} info Route's info
   */
  static treatDefAsArray(info) {
    const logics = info['auth'] === true
      ? [this.authenticate]
      : [];
    const size = info['def'].length - 1;

    /** Iterate through all method list */
    for (const [index, name] of info['def'].entries()) {
      logics.push(
        /** Push intermediate method to distinguish controller and middlewares */
        index === size
          /** The last method should be controller method */
          ? (req, res) => info['controller'][name]({req, res, models: info['models'], ...utils})
          /** The rest method should be middlewares */
          : (req, res, next) => info['middleware'][name]({req, res, next, models: info['models'], ...utils})
      )
    }

    /** Bind operation */
    this.app[this.getMethod(info['method'])](`${this.config.api.prefix}/${info['basename']}${info['path']}`, logics)
  }

  /**
   * Treat routes value as object
   *
   * @memberOf Builder
   * @static
   *
   * @param {object} info Route's info
   * @param {boolean} testing
   */
  static treatDefAsObject(info, testing = false) {
    let alteredInfo = null

    Object.keys(is('object', info['def']['handler']) ? info['def']['handler'] : {get: info['def']['handler']})
      .forEach((method) => {
        /** Alter routes info */
        info['method'] = method;
        info['auth'] = is('empty', info['def']['auth']) ? true : info['def']['auth'];
        info['def'] = is('object', info['def']['handler'])
          ? info['def']['handler'][method]
          : info['def']['handler']

        /** Check which type should handler treated as */
        is('string', info['def'])
          /** Handler is defined as string */
          ? this.treatDefAsString(info)
          /** Handler is defined as object */
          : this.treatDefAsArray(info)

        if (testing) alteredInfo = info
      })

    return alteredInfo
  }

  /**
   * Capitalize string
   *
   * @memberOf Builder
   * @static
   *
   * @param {string} value input string
   * @returns {string}
   */
  static capitalize(value) {
    return value.charAt(0).toUpperCase() + value.slice(1)
  }

  /**
   * Convert string to route method
   *
   * @memberOf Builder
   * @static
   *
   * @param {string} value Method string
   * @returns {string}
   */
  static getMethod(value) {
    return is('empty', value) ? 'get' : value.toLowerCase();
  }

  /**
   * Log catch statement
   *
   * @memberOf Builder
   * @static
   *
   * @param text
   * @param error
   */
  static error(text, error) {
    this.tools.stop('fail');
    log(`${text}${error}`);
  }
}
