export const schema = (DataTypes) => ({
  //Ex: name: DataTypes.STRING,
  //Place your table columns here
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true
  }
});

export const options = {};

export const associations = {
  user: 'belongsTo'
}
