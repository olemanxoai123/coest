export const schema = (DataTypes) => ({
  id: {
    type: DataTypes.STRING(8),
    primaryKey: true,
  },
  name: DataTypes.STRING(40),
  role: {
    type: DataTypes.STRING(7),
    defaultValue: 'Student'
  },
  email: DataTypes.STRING(100),
  password: DataTypes.STRING(100),
  class: DataTypes.STRING(10),
  expertise: DataTypes.STRING(40),
  faculty: {
    type: DataTypes.STRING(40),
    defaultValue: 'Information Technology'
  }
});

export const options = {};

export const associations = {
  container: 'hasMany',
  auth: 'hasMany'
}

