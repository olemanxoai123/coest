export const schema = (DataTypes) => ({
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  token: DataTypes.STRING,
  expiresAt: DataTypes.DATE
});

export const options = {
  indexes: [
    {
      unique: true,
      fields: ['id']
    }
  ]
};

export const associations = {
  user: 'belongsTo'
};

