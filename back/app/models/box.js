import moment from "moment";

export const schema = (DataTypes) => ({
  //Ex: name: DataTypes.STRING,
  //Place your table columns here
  code: {
    type: DataTypes.STRING(20),
    primaryKey: true
  },
  id: DataTypes.STRING(20),
  name: DataTypes.STRING(50),
  password: {
    type: DataTypes.STRING(20),
    default: ''
  },
  type: DataTypes.STRING(4),
  file: DataTypes.STRING,
  entry: DataTypes.STRING,
  isFile: DataTypes.BOOLEAN,
  stdin: DataTypes.BOOLEAN,
  isPublic: DataTypes.BOOLEAN,
  note: DataTypes.STRING,
  deadline: DataTypes.DATE
});

export const options = {};

export const associations = {
  user: 'belongsTo'
}
