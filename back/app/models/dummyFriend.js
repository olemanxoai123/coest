export const schema = (DataTypes) => ({
  id: {
    type: DataTypes.STRING(8),
    primaryKey: true,
  },
  text: DataTypes.STRING,
});

export const options = {};

export const associations = {
  dummy: 'belongsTo',
}

