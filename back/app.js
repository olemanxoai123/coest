import config from './config/config.js';
import boot from './config/express.js'
import {build} from './app/index.js'
import cli from './scripts/cli.js'

/** Create terminal decorate tool (optional) */
const tools = cli(!config()['server']['cli']);

/** Start */
tools.line(50, '=');

/** Building application */
const {app, httpServer} = await build(config());

/** Boot app */
await boot(app);

/** End of start */
tools.line(50, '=');

app.get('/', function (req, res) {
  res.send('Welcome!')
})

/** Start Server */
httpServer.listen(
  config().server.port,
  config().server.host,
  () => tools.serve(config().server)
)
