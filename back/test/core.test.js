import {expect} from "chai"
import sequelize from 'sequelize'
import {build, Builder} from '../app/index.js'
import config from '../config/config.js'
import {Server} from "socket.io"
import * as utils from "../scripts/utils.js"
import {makeBox, wipe} from "../scripts/box.js"
import {existsSync, readFileSync, mkdirSync, writeFileSync} from "fs"
import fse from "fs-extra"
import {execSync} from "child_process"


/**
 * Clone object
 *
 * @param object
 * @returns {*}
 */
function clone(object) {
  return typeof object === 'function' ? object : Object.assign({}, object)
}

/** Test config */
describe('config', () => {
  it('should load', () => {
    expect(config()).to.be.a('object', 'Config not loaded')
  })
})

/** Test Builder */
describe('Builder', () => {
  /** Test init() */
  describe('init()', () => {
    /** Call method */
    Builder.init(config())
    const app = clone(Builder.app)

    /** Target */
    it('should have Express app', () => {
      expect(app).to.be.a('function', 'App not initiated')
      expect(app.constructor.name).to.equal('EventEmitter', 'Express app not initiated')
    })
    it('should have Node HTTP server', () => {
      expect(Builder.httpServer).to.be.a('object', 'Server not initiated')
      expect(Builder.httpServer.constructor.name).to.equal('Server', 'Server not initiated')
    })
    it('should have config object', () => {
      expect(Builder.config).to.be.a('object', 'Config not initiated')
    })
  })

  /** Test buildResource() */
  describe('buildResource()', () => {
    let sq = new sequelize.Sequelize(config().sequelize);
    /** Sequelize should load */
    it('Sequelize should load', () => {
      expect(sq).to.be.an.instanceof(sequelize.Sequelize, 'Sequelize not loaded')
    })

    /** Build model */
    describe('buildModels()', async () => {
      /** Build model */
      it('Models should load', () => {
        return Builder.buildModels(sq)
          .then(() => {
            expect(sq.models).to.be.a('object')

            expect(sq.models['Dummy'].name).to.equal('Dummy', 'Model is not loaded')
            expect(sq.models['DummyFriend'].name).to.equal('DummyFriend', 'Model is not loaded')
          })
      })
      /** Build association */
      it('Associations should load', () => {
        return Builder.buildAssociations(sq)
          .then(() => {
            const associations1 = sq.models['Dummy'].associations
            const associations2 = sq.models['DummyFriend'].associations

            expect(associations1['DummyFriend']).to.not.be.null
            expect(associations2['Dummy']).to.not.be.null

            expect(associations1['DummyFriend'].associationType).to.equal(
              'HasOne',
              'Associations is not loaded'
            )
            expect(associations2['Dummy'].associationType).to.equal(
              'BelongsTo',
              'Associations is not loaded'
            )
          })
      })
    })
  })

  /** Test buildSocket() */
  describe('buildSocket()', () => {
    /** Build socket */
    it('Socket should load', () => {
      return Builder.buildSocket()
        .then(io => {
          expect(io.constructor.name).to.equal('Server', 'Constructor name is not Server')
          expect(io).to.be.an.instanceof(Server, 'Not instance of Socket.Server')
        })
    })
  })

  /** Test buildApp() */
  describe('buildApp()', () => {
    /** Test buildAuthentication() */
    describe('buildAuthentication()', () => {
      /** Test authLogic() */
      describe('authLogic()', () => {
        /** Fake data */
        let passer = null
        let auth = null

        const fakeAuth = (token, user) => ([
          {
            models: {
              Auth: {
                findOne: () => ({
                  then: callback => {
                    if (user === [] || user === null) {
                      return {catch: callback => callback()}
                    } else {
                      user['token'] = utils.hash(user['token'])
                      callback(user)
                      return {catch: () => true}
                    }
                  },
                })
              }
            }
          },
          token,
          data => passer = data,
          () => auth = true,
          () => auth = false
        ])

        /** Case 1: No token in headers */
        it('no token', () => {
          const sample = {id: '1', auth: '1', token: 'abc', 'User.role': 'Teacher'}
          Builder.authLogic(...fakeAuth(null, sample))

          expect(auth).to.be.false
          expect(passer).to.be.null
        })

        /** Case 2: Header token === User token */
        it('equal token', () => {
          const sample = {id: '1', auth: '1', token: 'abc', 'User.role': 'Teacher'}
          Builder.authLogic(...fakeAuth('2|abc', sample))

          expect(auth).to.be.true
          expect(passer).to.deep.equal({id: '1', auth: '1', role: 'Teacher'})
        })

        /** Case 3: Header token !== User token */
        it('not equal token', () => {
          const sample = {id: '1', auth: '1', token: 'abc', 'User.role': 'Teacher'}
          Builder.authLogic(...fakeAuth('2|aabc', sample))

          expect(auth).to.be.false
        })

        /** Case 4: Header token !== User token */
        it('empty query', () => {
          Builder.authLogic(...fakeAuth('2|aabc', null))

          expect(auth).to.be.false
        })
      })

      /** Test authentication middleware is loaded */
      it('Authentication middleware should load', () => {
        let holder = null
        const fakeIO = {use: something => holder = something}

        return Builder.buildAuthentication(fakeIO, null)
          .then(() => {
            expect(Builder.authenticate).to.be.a('function', 'Authentication middleware not loaded common')
            expect(holder).to.be.a('function', 'Authentication middleware not loaded for Socket')
          })
      })
    })

    /** Test buildRoutes() */
    describe('buildRoutes()', () => {
      /** Test bindRoutes() */
      describe('bindRoutes()', () => {
        /** Fake cli tools */
        Builder.tools = {stop: () => true}
        Builder.config = config()

        /** Fake app */
        let route = null
        let method = null
        let callbacks = null
        const reset = () => {
          route = null
          method = null
          callbacks = null
        }
        const compare = (actual, expected) => {
          expect(JSON.stringify(actual)).to.equal(JSON.stringify(expected))
          /** Expect controller function to be kept intact */
          Object.keys(actual['controller'])
            .forEach(key => expect(actual['controller'][key].name).to.equal(expected['controller'][key].name))
          /** Expect controller function to be kept intact */
          Object.keys(actual['middleware'])
            .forEach(key => expect(actual['middleware'][key].name).to.equal(expected['middleware'][key].name))
        }


        /** Fake all route method for testing */
        Builder.app = {
          get: (actualRoute, ...actualCallbacks) => {
            route = actualRoute
            callbacks = actualCallbacks
            method = 'get'
          },
          post: (actualRoute, ...actualCallbacks) => {
            route = actualRoute
            callbacks = actualCallbacks
            method = 'post'
          },
          put: (actualRoute, ...actualCallbacks) => {
            route = actualRoute
            callbacks = actualCallbacks
            method = 'put'
          },
          delete: (actualRoute, ...actualCallbacks) => {
            route = actualRoute
            callbacks = actualCallbacks
            method = 'delete'
          }
        }

        /** Test treatDefAsString() */
        describe('treatDefAsString()', () => {
          it('controller + auth', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint1',
              basename: 'dummy',
              def: 'test',
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => ({}), config: () => ({})}
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsString(info)

            expect(route).to.equal('/api/dummy/endpoint1', 'Incorrect path')
            expect(method).to.equal('get', 'Incorrect method')

            expect(callbacks[0]).to.be.null // Builder.authenticate is not created, so it has to be null
            expect(callbacks[1]).to.be.a('function', 'Logic is not loaded')

            callbacks[1]()

            expect(holder).to.be.true
          })
          it('controller - auth', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint2',
              basename: 'dummy',
              def: 'hello',
              models: null,
              auth: false,
              controller: {test: () => ({}), hello: () => holder = true},
              middleware: {log: () => ({}), config: () => ({})}
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsString(info)

            expect(route).to.equal('/api/dummy/endpoint2', 'Incorrect path')
            expect(method).to.equal('get', 'Incorrect method')
            expect(callbacks[1]).to.be.a('function', 'Logic is not loaded')

            /** Test for correct controller selection */
            callbacks[1]()

            expect(holder).to.be.true
          })
          it('controller + auth + [method]', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint1',
              basename: 'dummy',
              def: 'test',
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => ({}), config: () => ({})},
              method: 'post'
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsString(info)

            expect(route).to.equal('/api/dummy/endpoint1', 'Incorrect path')
            expect(method).to.equal('post', 'Incorrect method')
            expect(callbacks[0]).to.be.null // Builder.authenticate is not created, so it has to be null
            expect(callbacks[1]).to.be.a('function', 'Logic is not loaded')

            callbacks[1]()

            expect(holder).to.be.true
          })
          it('controller - auth + [method]', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint1',
              basename: 'dummy',
              def: 'test',
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => ({}), config: () => ({})},
              method: 'put'
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsString(info)

            expect(route).to.equal('/api/dummy/endpoint1', 'Incorrect path')
            expect(method).to.equal('put', 'Incorrect method')
            expect(callbacks[0]).to.be.null // Builder.authenticate is not created, so it has to be null
            expect(callbacks[1]).to.be.a('function', 'Logic is not loaded')

            callbacks[1]()

            expect(holder).to.be.true
          })
        })

        /** Test treatDefAsArray() */
        describe('treatDefAsArray()', () => {
          it('controller + auth - middleware', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint3',
              basename: 'dummy',
              def: ['test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => ({}), config: () => ({})}
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint3', 'Incorrect path')
            expect(method).to.equal('get', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(2)
            expect(callbacks[0][0]).to.be.null
            expect(callbacks[0][1]).to.be.a('function', 'Logic is not loaded')

            /** Test for correct controller selection */
            callbacks[0][1]()

            expect(holder).to.be.true
          })
          it('controller - auth - middleware', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint4',
              basename: 'dummy',
              def: ['test'],
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint4', 'Incorrect path')
            expect(method).to.equal('get', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(1)
            /** Auth middleware should be placed first */
            expect(callbacks[0][0]).to.be.a('function')

            callbacks[0][0]()

            /** The only callback is controller */
            expect(holder).to.be.true
          })
          it('controller + auth + middleware', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint4',
              basename: 'dummy',
              def: ['log', 'config', 'test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint4', 'Incorrect path')
            expect(method).to.equal('get', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(4)
            /** Auth middleware should be placed first */
            expect(callbacks[0][0]).to.be.null

            callbacks[0].shift()

            /** Test for method order (middleware should be placed after auth middleware) */
            const order = ['log', 'config', true]
            callbacks[0].forEach((v, k) => {
              v()
              expect(holder).to.equal(order[k])
            })
          })
          it('controller - auth + middleware', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: ['log', 'config', 'test'],
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint6', 'Incorrect path')
            expect(method).to.equal('get', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(3)

            /** Test for method order (auth middleware is not included) */
            const order = ['log', 'config', true]
            callbacks[0].forEach((v, k) => {
              v()
              expect(holder).to.equal(order[k])
            })
          })
          it('controller + auth - middleware + [method]', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint3',
              basename: 'dummy',
              def: ['test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => ({}), config: () => ({})},
              method: 'post'
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint3', 'Incorrect path')
            expect(method).to.equal('post', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(2)
            expect(callbacks[0][0]).to.be.null
            expect(callbacks[0][1]).to.be.a('function', 'Logic is not loaded')

            /** Test for correct controller selection */
            callbacks[0][1]()

            expect(holder).to.be.true
          })
          it('controller - auth - middleware + [method]', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint4',
              basename: 'dummy',
              def: ['test'],
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'put'
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint4', 'Incorrect path')
            expect(method).to.equal('put', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(1)
            /** Auth middleware should be placed first */
            expect(callbacks[0][0]).to.be.a('function')

            callbacks[0][0]()

            /** The only callback is controller */
            expect(holder).to.be.true
          })
          it('controller + auth + middleware + [method]', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint4',
              basename: 'dummy',
              def: ['log', 'config', 'test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'delete'
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint4', 'Incorrect path')
            expect(method).to.equal('delete', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(4)
            /** Auth middleware should be placed first */
            expect(callbacks[0][0]).to.be.null

            callbacks[0].shift()

            /** Test for method order (middleware should be placed after auth middleware) */
            const order = ['log', 'config', true]
            callbacks[0].forEach((v, k) => {
              v()
              expect(holder).to.equal(order[k])
            })
          })
          it('controller - auth + middleware + [method]', () => {
            /** Fake info */
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: ['log', 'config', 'test'],
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'post'
            }

            reset()
            /** Invoke */
            Builder.authenticate = null
            Builder.treatDefAsArray(info)

            expect(route).to.equal('/api/dummy/endpoint6', 'Incorrect path')
            expect(method).to.equal('post', 'Incorrect method')
            expect(callbacks[0]).to.have.lengthOf(3)

            /** Test for method order (auth middleware is not included) */
            const order = ['log', 'config', true]
            callbacks[0].forEach((v, k) => {
              v()
              expect(holder).to.equal(order[k])
            })
          })
        })

        /** Test treatDefAsObject() */
        describe('treatDefAsObject()', () => {
          it('controller(object + string) + auth + [method]', () => {
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: {
                handler: {
                  post: 'test',
                },
              },
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }
            /** Expected info */
            const expected = {
              path: '/endpoint6',
              basename: 'dummy',
              def: 'test',
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'post'
            }
            /** Alter the info and call treatDefAsString() or treatDefAsArray */
            const actual = Builder.treatDefAsObject(info, true)

            compare(actual, expected)
          })
          it('controller(object + string) - auth + [method]', () => {
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: {
                handler: {
                  delete: 'test',
                },
                auth: false
              },
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }
            /** Expected info */
            const expected = {
              path: '/endpoint6',
              basename: 'dummy',
              def: 'test',
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'delete'
            }
            /** Alter the info and call treatDefAsString() or treatDefAsArray */
            const actual = Builder.treatDefAsObject(info, true)

            compare(actual, expected)
          })
          it('controller(object + array) + auth - middleware + [method]', () => {
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: {
                handler: {
                  put: ['test'],
                },
              },
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }
            /** Expected info */
            const expected = {
              path: '/endpoint6',
              basename: 'dummy',
              def: ['test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'put'
            }
            /** Alter the info and call treatDefAsString() or treatDefAsArray */
            const actual = Builder.treatDefAsObject(info, true)

            compare(actual, expected)
          })
          it('controller(object + array) - auth - middleware + [method]', () => {
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: {
                handler: {
                  post: ['test'],
                },
                auth: false
              },
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }
            /** Expected info */
            const expected = {
              path: '/endpoint6',
              basename: 'dummy',
              def: ['test'],
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'post'
            }
            /** Alter the info and call treatDefAsString() or treatDefAsArray */
            const actual = Builder.treatDefAsObject(info, true)

            compare(actual, expected)
          })
          it('controller(object + array) + auth + middleware + [method]', () => {
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: {
                handler: {
                  post: ['log', 'test'],
                },
              },
              models: null,
              auth: false,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }
            /** Expected info */
            const expected = {
              path: '/endpoint6',
              basename: 'dummy',
              def: ['log', 'test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'post'
            }
            /** Alter the info and call treatDefAsString() or treatDefAsArray */
            const actual = Builder.treatDefAsObject(info, true)

            compare(actual, expected)
          })
          it('controller(object + array) - auth + middleware + [method]', () => {
            let holder = null
            const info = {
              path: '/endpoint6',
              basename: 'dummy',
              def: {
                handler: {
                  post: ['log', 'test'],
                },
                auth: true
              },
              models: null,
              auth: null,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'}
            }
            /** Expected info */
            const expected = {
              path: '/endpoint6',
              basename: 'dummy',
              def: ['log', 'test'],
              models: null,
              auth: true,
              controller: {test: () => holder = true, hello: () => ({})},
              middleware: {log: () => holder = 'log', config: () => holder = 'config'},
              method: 'post'
            }
            /** Alter the info and call treatDefAsString() or treatDefAsArray */
            const actual = Builder.treatDefAsObject(info, true)

            compare(actual, expected)
          })
        })

        /** Test bindRoutes */
        it('working properly', () => {
          const routes = {
            '/endpoint1': ['log', 'test'],
            '/endpoint2': 'handle',
            '/endpoint3': {
              handler: {
                post: ['config', 'handle'],
              },
              auth: false
            }
          }
          const expected = [
            {
              path: '/endpoint1',
              basename: 'dummy',
              def: ['log', 'test'],
              models: null,
              auth: true,
              controller: {
                test() {
                  console.log('test function')
                },

                handle() {
                  console.log('handle function')
                },
              },
              middleware: {
                log({next}) {
                  console.log('This is middleware')

                  next();
                },

                config({next}) {
                  console.log('This is middleware for socket')

                  next();
                }
              }
            },
            {
              path: '/endpoint2',
              basename: 'dummy',
              def: 'handle',
              models: null,
              auth: true,
              controller: {
                test() {
                  console.log('test function')
                },

                handle() {
                  console.log('handle function')
                },
              },
              middleware: {
                log({next}) {
                  console.log('This is middleware')

                  next();
                },

                config({next}) {
                  console.log('This is middleware for socket')

                  next();
                }
              }
            },
            {
              path: '/endpoint3',
              basename: 'dummy',
              def: {
                handler: {
                  post: ['config', 'handle'],
                },
                auth: false
              },
              models: null,
              auth: false,
              controller: {
                test() {
                  console.log('test function')
                },

                handle() {
                  console.log('handle function')
                },
              },
              middleware: {
                log({next}) {
                  console.log('This is middleware')

                  next();
                },

                config({next}) {
                  console.log('This is middleware for socket')

                  next();
                }
              },
            }
          ]
          return Builder.bindRoutes(routes, 'dummy.js', null, true)
            .then(actual => {
              expect(JSON.stringify(actual)).to.equal(JSON.stringify(expected))
              actual.forEach((v, k) => compare(actual[k], expected[k]))
            })
        })
      })
    })
  })
})

/** Test utils */
describe('utils', () => {
  /** Test is() */
  describe('is()', () => {
    it('case: array', () => {
      const sample = [1, 2, 3]

      /** Can detect array */
      expect(utils.is('array', sample)).to.be.true
      /** Distinguish array and object  */
      expect(utils.is('object', sample)).to.be.false
      /** Not other primitives */
      expect(utils.is('string', sample)).to.be.false
      expect(utils.is('number', sample)).to.be.false
      expect(utils.is('boolean', sample)).to.be.false
    })
    it('case: object', () => {
      const sample = {a: 1, b: 2, c: 3}

      /** Can detect object */
      expect(utils.is('object', sample)).to.be.true
      /** Distinguish array and object  */
      expect(utils.is('array', sample)).to.be.false
      /** Not other primitives */
      expect(utils.is('string', sample)).to.be.false
      expect(utils.is('number', sample)).to.be.false
      expect(utils.is('boolean', sample)).to.be.false
    })
    it('case: primitives', () => {
      /** Can detect string */
      expect(utils.is('string', 'abc')).to.be.true
      expect(utils.is('number', 'abc')).to.be.false
      expect(utils.is('boolean', 'abc')).to.be.false

      /** Can detect number */
      expect(utils.is('string', 123)).to.be.false
      expect(utils.is('number', 123)).to.be.true
      expect(utils.is('boolean', 123)).to.be.false

      /** Can detect boolean */
      expect(utils.is('string', true)).to.be.false
      expect(utils.is('number', true)).to.be.false
      expect(utils.is('boolean', true)).to.be.true
    })
  })

  /** Test craft() */
  describe('craft()', () => {
    it('case 1', () => {
      const expected = {success: true, data: {a: 1, b: 2}, message: 'This is message'}

      expect(utils.craft(true, {a: 1, b: 2}, 'This is message')).to.be.deep.equal(expected)
    })
    it('case 2', () => {
      const expected = {success: false, data: {a: 1, b: 2}, message: ''}

      expect(utils.craft(false, {a: 1, b: 2})).to.be.deep.equal(expected)
    })
    it('case 3', () => {
      const expected = {success: true, data: {}, message: ''}

      expect(utils.craft(true, {}, '')).to.be.deep.equal(expected)
    })
  })

  /** Test craft() */
  describe('exclude()', () => {
    it('working properly', () => {
      const sample = {a: 1, b: 2, c: 3}

      /** Object excluded */
      expect(utils.exclude(sample, ['a', 'b'])).to.be.deep.equal({c: 3})
      /** Origin object is kept intact */
      expect(sample).to.be.deep.equal({a: 1, b: 2, c: 3})
    })
  })

  /** Test only() */
  describe('only()', () => {
    it('working properly', () => {
      const sample = {a: 1, b: 2, c: 3}

      /** Object excluded */
      expect(utils.only(sample, ['a'])).to.be.deep.equal({a: 1})
      /** Origin object is kept intact */
      expect(sample).to.be.deep.equal({a: 1, b: 2, c: 3})
    })
  })
})

/** Test Box */
describe('Box', () => {
  const prefix = './storage/app/containers/'
  const source = {
    cpp: '#include <stdio.h>\n' +
      '#include <string.h>\n' +
      '\n' +
      'int main(int argc, char** argv)\n' +
      '{\n' +
      '    if (argc == 1) {\n' +
      '        char *first = NULL;\n' +
      '        char *second = NULL;\n' +
      '        \n' +
      '        size_t firstLen = 0;\n' +
      '        size_t secondLen = 0;\n' +
      '\n' +
      '        getline(&first, &firstLen, stdin);\n' +
      '        getline(&second, &secondLen, stdin);\n' +
      '\n' +
      '        first[strcspn(first,"\\n")] = 0;\n' +
      '        second[strcspn(second,"\\n")] = 0;\n' +
      '\n' +
      '        printf("%s+++%s\\\n", first, second);\n' +
      '    } else {\n' +
      '        printf("%s---%s\\\n", argv[1], argv[2]);\n' +
      '    }\n' +
      '\n' +
      '    return 0;\n' +
      '}',
    java: 'import java.util.Scanner;\n' +
      '\n' +
      'public class HelloWorld {\n' +
      '   public static void main(String[] args) {\n' +
      '      try {\n' +
      '         System.out.println(args[0] + "---" + args[1]);\n' +
      '      } catch (Exception e) {\n' +
      '         Scanner sc = new Scanner(System.in);\n' +
      '\n' +
      '         String first = sc.nextLine();\n' +
      '         String second = sc.nextLine();\n' +
      '\n' +
      '         System.out.println(first + "+++" + second);\n' +
      '\n' +
      '         sc.close();\n' +
      '      }\n' +
      '   }\n' +
      '}\n'
  }

  // /** Test makeBox() */
  // describe('makeBox()', () => {
  //   it('should create Java Box', () => {
  //     /** Make box */
  //     makeBox('123_java', 'java', false)
  //     expect(existsSync(`${prefix}_123_java`)).to.be.true
  //
  //     /** Check type */
  //     const dockerfile = readFileSync(`${prefix}_123_java/Dockerfile`, {encoding: "utf-8"})
  //     expect(dockerfile.includes('coest-java')).to.be.true
  //
  //     /** Prep for later */
  //     mkdirSync(`${prefix}_123_java/src`)
  //     writeFileSync(`${prefix}_123_java/src/HelloWorld.java`, source.java)
  //   })
  //   it('should create C++ Box', () => {
  //     /** Make box */
  //     makeBox('123_cpp', 'cpp', false)
  //     expect(existsSync(`${prefix}_123_cpp`)).to.be.true
  //
  //     /** Check type */
  //     const dockerfile = readFileSync(`${prefix}_123_cpp/Dockerfile`, {encoding: "utf-8"})
  //     expect(dockerfile.includes('coest-cpp')).to.be.true
  //
  //     /** Prep for later */
  //     mkdirSync(`${prefix}_123_cpp/src`)
  //     writeFileSync(`${prefix}_123_cpp/src/HelloWorld.cpp`, source.cpp)
  //   })
  //   it('should create C Box', () => {
  //     /** Make box */
  //     makeBox('123_c', 'c', false)
  //     expect(existsSync(`${prefix}_123_c`)).to.be.true
  //
  //     /** Check type */
  //     const dockerfile = readFileSync(`${prefix}_123_c/Dockerfile`, {encoding: "utf-8"})
  //     expect(dockerfile.includes('coest-cpp')).to.be.true
  //
  //     /** Prep for later */
  //     mkdirSync(`${prefix}_123_c/src`)
  //     writeFileSync(`${prefix}_123_c/src/HelloWorld.c`, source.cpp)
  //   })
  // })
  //
  // /** Test clone() */
  // describe('clone()', () => {
  //   it('should clone Box', () => {
  //     makeBox('123_java', 'java', false).clone('cloned')
  //     expect(existsSync(`${prefix}_123_java-cloned`)).to.be.true
  //   })
  // })
  //
  // /** Test craft() */
  // describe('craft()', () => {
  //   it('should create archive.zip', () => {
  //     /** Zip file */
  //     const chunks = [
  //       [
  //         80, 75, 3, 4, 20, 0, 0, 0, 8, 0, 203, 96,
  //         234, 82, 243, 25, 64, 52, 31, 0, 0, 0, 45, 0,
  //         0, 0, 15, 0, 0, 0, 50, 45, 119, 114, 97, 112,
  //         45, 108, 105, 110, 101, 46, 116, 120, 116, 83, 74, 204,
  //         200, 207, 200, 87, 72, 204, 0, 66, 37, 5, 32, 47,
  //         51, 35, 83, 73, 1, 89, 84, 87, 87, 23, 44, 26,
  //         147, 167, 4, 0, 80, 75, 3, 4, 20, 0, 0, 0,
  //         8, 0, 184, 97, 234, 82, 113, 151, 128, 0, 24, 0,
  //         0, 0, 29, 0
  //       ],
  //       [
  //         0, 0, 17, 0, 0, 0, 51, 45, 110, 111, 119, 114,
  //         97, 112, 45, 102, 105, 108, 101, 46, 116, 120, 116, 75,
  //         204, 200, 204, 200, 228, 229, 74, 204, 72, 205, 72, 229,
  //         229, 2, 243, 116, 117, 117, 193, 220, 152, 60, 0, 80,
  //         75, 3, 4, 20, 0, 0, 0, 8, 0, 180, 97, 234,
  //         82, 61, 94, 105, 107, 31, 0, 0, 0, 47, 0, 0,
  //         0, 15, 0, 0, 0, 52, 45, 119, 114, 97, 112, 45,
  //         102, 105, 108, 101, 46, 116, 120, 116, 83, 74, 204, 200,
  //         207, 200, 87, 72
  //       ],
  //       [
  //         204, 0, 66, 37, 94, 46, 32, 55, 51, 35, 19, 194,
  //         128, 139, 235, 234, 234, 130, 133, 99, 242, 148, 0, 80,
  //         75, 3, 4, 20, 0, 0, 0, 8, 0, 224, 96, 234,
  //         82, 171, 191, 16, 100, 22, 0, 0, 0, 27, 0, 0,
  //         0, 17, 0, 0, 0, 49, 45, 110, 111, 119, 114, 97,
  //         112, 45, 108, 105, 110, 101, 46, 116, 120, 116, 75, 204,
  //         200, 204, 200, 84, 72, 204, 72, 205, 72, 85, 0, 179,
  //         117, 117, 117, 193, 188, 152, 60, 0, 80, 75, 1, 2,
  //         31, 0, 20, 0
  //       ],
  //       [
  //         0, 0, 8, 0, 203, 96, 234, 82, 243, 25, 64, 52,
  //         31, 0, 0, 0, 45, 0, 0, 0, 15, 0, 36, 0,
  //         0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 0,
  //         0, 0, 50, 45, 119, 114, 97, 112, 45, 108, 105, 110,
  //         101, 46, 116, 120, 116, 10, 0, 32, 0, 0, 0, 0,
  //         0, 1, 0, 24, 0, 48, 46, 133, 83, 73, 117, 215,
  //         1, 137, 212, 102, 180, 73, 117, 215, 1, 95, 245, 211,
  //         248, 122, 116, 215, 1, 80, 75, 1, 2, 31, 0, 20,
  //         0, 0, 0, 8
  //       ],
  //       [
  //         0, 184, 97, 234, 82, 113, 151, 128, 0, 24, 0, 0,
  //         0, 29, 0, 0, 0, 17, 0, 36, 0, 0, 0, 0,
  //         0, 0, 0, 32, 0, 0, 0, 76, 0, 0, 0, 51,
  //         45, 110, 111, 119, 114, 97, 112, 45, 102, 105, 108, 101,
  //         46, 116, 120, 116, 10, 0, 32, 0, 0, 0, 0, 0,
  //         1, 0, 24, 0, 36, 9, 185, 93, 74, 117, 215, 1,
  //         36, 9, 185, 93, 74, 117, 215, 1, 154, 73, 103, 180,
  //         73, 117, 215, 1, 80, 75, 1, 2, 31, 0, 20, 0,
  //         0, 0, 8, 0
  //       ],
  //       [
  //         180, 97, 234, 82, 61, 94, 105, 107, 31, 0, 0, 0,
  //         47, 0, 0, 0, 15, 0, 36, 0, 0, 0, 0, 0,
  //         0, 0, 32, 0, 0, 0, 147, 0, 0, 0, 52, 45,
  //         119, 114, 97, 112, 45, 102, 105, 108, 101, 46, 116, 120,
  //         116, 10, 0, 32, 0, 0, 0, 0, 0, 1, 0, 24,
  //         0, 65, 130, 224, 88, 74, 117, 215, 1, 65, 130, 224,
  //         88, 74, 117, 215, 1, 147, 52, 83, 194, 73, 117, 215,
  //         1, 80, 75, 1, 2, 31, 0, 20, 0, 0, 0, 8,
  //         0, 224, 96, 234
  //       ],
  //       [
  //         82, 171, 191, 16, 100, 22, 0, 0, 0, 27, 0, 0,
  //         0, 17, 0, 36, 0, 0, 0, 0, 0, 0, 0, 32,
  //         0, 0, 0, 223, 0, 0, 0, 49, 45, 110, 111, 119,
  //         114, 97, 112, 45, 108, 105, 110, 101, 46, 116, 120, 116,
  //         10, 0, 32, 0, 0, 0, 0, 0, 1, 0, 24, 0,
  //         171, 68, 37, 106, 73, 117, 215, 1, 101, 226, 91, 68,
  //         74, 117, 215, 1, 155, 223, 192, 89, 73, 117, 215, 1,
  //         80, 75, 5, 6, 0, 0, 0, 0, 4, 0, 4, 0,
  //         136, 1, 0, 0
  //       ],
  //       [36, 1, 0, 0, 0, 0]
  //     ]
  //
  //     /** Should create file */
  //     chunks.forEach(chunk => {
  //       makeBox('123_java', 'java', false).craft(chunk)
  //       makeBox('123_cpp', 'cpp', false).craft(chunk)
  //       makeBox('123_c', 'c', false).craft(chunk)
  //     })
  //     expect(existsSync(`${prefix}_123_java/archive.zip`)).to.be.true
  //
  //     /** File should be kept intact */
  //     const content = readFileSync(`${prefix}_123_java/archive.zip`)
  //     expect(content).to.be.deep.equal(Buffer.from([].concat.apply([], chunks)))
  //   })
  //   it('should create archive.txt', () => {
  //     /** Txt file */
  //     const chunks = [
  //       [
  //         34, 97, 104, 105, 104, 105, 32, 97, 104, 101, 104, 101,
  //         34, 32, 34, 97, 104, 111, 104, 111, 34, 32, 34, 97,
  //         104, 105, 104, 105, 32, 97, 104, 101, 104, 101, 45, 45,
  //         45, 97, 104, 111, 104, 111, 92, 110, 34, 13, 10, 34,
  //         97, 104, 111, 104, 111, 32, 97, 104, 97, 104, 97, 34,
  //         32, 34, 97, 104, 105, 104, 105, 34, 32, 34, 97, 104,
  //         111, 104, 111, 32, 97, 104, 97, 104, 97, 45, 45, 45,
  //         97, 104, 105, 104, 105, 92, 110, 34, 13, 10, 34, 105,
  //         104, 97, 104, 97
  //       ],
  //       [
  //         32, 105, 104, 111, 104, 111, 34, 32,
  //         34, 105, 104, 105, 104, 105, 34, 32,
  //         34, 105, 104, 97, 104, 97, 32, 105,
  //         104, 111, 104, 111, 45, 45, 45, 105,
  //         104, 105, 104, 105, 92, 110, 34
  //       ]
  //     ]
  //
  //     /** Should create file */
  //     chunks.forEach(chunk => makeBox('123_java-cloned', 'java', false).craft(chunk, 'txt'))
  //     expect(existsSync(`${prefix}_123_java-cloned/archive.txt`)).to.be.true
  //
  //     /** File should be kept intact */
  //     const content = readFileSync(`${prefix}_123_java-cloned/archive.txt`)
  //     expect(content).to.be.deep.equal(Buffer.from([].concat.apply([], chunks)))
  //   })
  // })
  //
  // /** Test extracted */
  // describe('extract()', () => {
  //   it('should extract archived zip', () => (
  //     (async () => {
  //       await makeBox('123_java', 'java', false).extract(1)
  //       await makeBox('123_cpp', 'cpp', false).extract(1)
  //       await makeBox('123_c', 'c', false).extract(1)
  //     })()
  //       .then(() => {
  //         it('should extract archive.zip', () => {
  //           expect(existsSync(`${prefix}_123_java/archive.zip`)).to.be.false
  //           expect(existsSync(`${prefix}_123_java/archive/1-nowrap-line.txt`)).to.be.true
  //           expect(existsSync(`${prefix}_123_java/archive/2-wrap-line.txt`)).to.be.true
  //           expect(existsSync(`${prefix}_123_java/archive/3-nowrap-file.txt`)).to.be.true
  //           expect(existsSync(`${prefix}_123_java/archive/4-wrap-file.txt`)).to.be.true
  //         })
  //       })
  //   ))
  //   it('should only moved archive txt', () => (
  //     makeBox('123_java-cloned', 'java', false).extract(1)
  //       .then(() => {
  //         expect(existsSync(`${prefix}_123_java-cloned/archive.txt`)).to.be.false
  //         expect(existsSync(`${prefix}_123_java-cloned/archive/archive.txt`)).to.be.true
  //       })
  //   ))
  // })
  //
  // //----------------------------------------------------------------------------------
  // // Turn on Docker
  // //----------------------------------------------------------------------------------
  //
  // /** Test build() */
  // describe('build()', () => {
  //   /** Build Java Box */
  //   describe('case: Java', () => {
  //     it('should create .sh scripts', () => {
  //       makeBox('123_java', 'java', false).build('HelloWorld')
  //       const scripts = {
  //         compile: '#!/bin/bash\n' +
  //           'find -name "*.java" > sources.txt\n' +
  //           'javac @sources.txt\n' +
  //           'rm sources.txt',
  //         execute: '#!/bin/bash\n' +
  //           'IFS="${SEP}" read -r -a array <<< "$ARGS"\n' +
  //           'java HelloWorld"${array[@]}"'
  //       }
  //
  //       /** Expect for compile script */
  //       expect(existsSync(`${prefix}_123_java/src-compile.sh`)).to.be.true
  //       expect(readFileSync(
  //         `${prefix}_123_java/src-compile.sh`,
  //         {encoding: 'utf-8'})
  //       ).to.equal(scripts.compile)
  //       /** Expect for execute script */
  //       expect(existsSync(`${prefix}_123_java/src-execute.sh`)).to.be.true
  //       expect(readFileSync(
  //         `${prefix}_123_java/src-execute.sh`,
  //         {encoding: 'utf-8'})
  //       ).to.equal(scripts.execute)
  //     }).timeout('10000')
  //     it('should create Docker image', () => {
  //       const id = execSync(
  //         'docker images --filter=reference=i123_java --format "{{.ID}}"',
  //         {encoding: 'utf-8'}
  //       )
  //
  //       expect(id).to.be.not.empty
  //     })
  //   })
  //
  //   /** Build C++ Box */
  //   describe('case: C++', () => {
  //     it('should create .sh scripts', () => {
  //       makeBox('123_cpp', 'cpp', false).build('HelloWorld')
  //       const scripts = {
  //         compile: '#!/bin/bash\n' +
  //           'g++ HelloWorld.cpp -o HelloWorld.o',
  //         execute: '#!/bin/bash\n' +
  //           'IFS="${SEP}" read -r -a array <<< "$ARGS"\n' +
  //           './HelloWorld.o"${array[@]}"'
  //       }
  //
  //       /** Expect for compile script */
  //       expect(existsSync(`${prefix}_123_cpp/src-compile.sh`)).to.be.true
  //       expect(readFileSync(
  //         `${prefix}_123_cpp/src-compile.sh`,
  //         {encoding: 'utf-8'})
  //       ).to.equal(scripts.compile)
  //       /** Expect for execute script */
  //       expect(existsSync(`${prefix}_123_cpp/src-execute.sh`)).to.be.true
  //       expect(readFileSync(
  //         `${prefix}_123_cpp/src-execute.sh`,
  //         {encoding: 'utf-8'})
  //       ).to.equal(scripts.execute)
  //     }).timeout('10000')
  //     it('should create Docker image', () => {
  //       const id = execSync(
  //         'docker images --filter=reference=i123_cpp --format "{{.ID}}"',
  //         {encoding: 'utf-8'}
  //       )
  //
  //       expect(id).to.be.not.empty
  //     })
  //   })
  //
  //   /** Build C Box */
  //   describe('case: C', () => {
  //     it('should create .sh scripts', () => {
  //       makeBox('123_c', 'c', false).build('HelloWorld')
  //       const scripts = {
  //         compile: '#!/bin/bash\n' +
  //           'gcc HelloWorld.c -o HelloWorld.o',
  //         execute: '#!/bin/bash\n' +
  //           'IFS="${SEP}" read -r -a array <<< "$ARGS"\n' +
  //           './HelloWorld.o"${array[@]}"'
  //       }
  //
  //       /** Expect for compile script */
  //       expect(existsSync(`${prefix}_123_c/src-compile.sh`)).to.be.true
  //       expect(readFileSync(
  //         `${prefix}_123_c/src-compile.sh`,
  //         {encoding: 'utf-8'})
  //       ).to.equal(scripts.compile)
  //       /** Expect for execute script */
  //       expect(existsSync(`${prefix}_123_c/src-execute.sh`)).to.be.true
  //       expect(readFileSync(
  //         `${prefix}_123_c/src-execute.sh`,
  //         {encoding: 'utf-8'})
  //       ).to.equal(scripts.execute)
  //     }).timeout('10000')
  //     it('should create Docker image', () => {
  //       const id = execSync(
  //         'docker images --filter=reference=i123_c --format "{{.ID}}"',
  //         {encoding: 'utf-8'}
  //       )
  //
  //       expect(id).to.be.not.empty
  //     })
  //   })
  // })

  /** Test exec */
  describe('exec()', () => {
    /** Both wrapped and unwrapped will be included */
    const payload = [
      {
        input: 'ahihi ahehe',
        output: 'ahihi---ahehe\n',
        error: false,
        expect: 'ahihi---ahehe\n',
      },
      {
        input: 'ahihi ahehe',
        output: 'ahihi+++ahehe\n',
        error: false,
        expect: 'ahihi---ahehe\n',
      },
      {
        input: '"ahoho ahaha" "ahihi"',
        output: 'ahoho ahaha---ahihi\n',
        error: false,
        expect: 'ahoho ahaha---ahihi\n',
      },
      {
        input: '"ahoho ahaha" "ahihi"',
        output: 'ahoho ahaha+++ahihi\n',
        error: false,
        expect: 'ahoho ahaha---ahihi\n',
      },
      {
        input: 'ahihi\nahehe',
        output: 'ahihi---ahehe\n',
        error: false,
        expect: 'ahihi---ahehe\n',
      },
      {
        input: 'ahihi\nahehe',
        output: 'ahihi+++ahehe\n',
        error: false,
        expect: 'ahihi---ahehe\n',
      },
      {
        input: '"ahoho ahaha"\n"ahihi"',
        output: 'ahoho ahaha---ahihi\n',
        error: false,
        expect: 'ahoho ahaha---ahihi\n',
      },
      {
        input: '"ahoho ahaha"\n"ahihi"',
        output: 'ahoho ahaha+++ahihi\n',
        error: false,
        expect: 'ahoho ahaha---ahihi\n',
      }
    ]

    /** Test executing Java Box */
    describe('Java', () => {
      it('isFile: 0 - stdin: 0', () => {
        makeBox('123_java', 'java', false).exec(
          pack => {
            if (pack.filename === '1-nowrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[0])
            } else if (pack.filename === '2-wrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[2])
            }
          },
          () => true,
          {stdin: 0, isFile: 0}
        )
      }).timeout('10000')
      it('isFile: 0 - stdin: 1', () => {
        makeBox('123_java', 'java', false).exec(
          pack => {
            if (pack.filename === '1-nowrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[1])
            } else if (pack.filename === '2-wrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[3])
            }
          },
          () => true,
          {stdin: 1, isFile: 0}
        )
      }).timeout('10000')
      it('isFile: 1 - stdin: 0', () => {
        let packs = []
        makeBox('123_java', 'java', false).exec(
          pack => packs.push(pack),
          () => true,
          {stdin: 0, isFile: 1}
        )

        packs.forEach(pack => {
          if (pack.filename === '3-nowrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[4])
          } else if (pack.filename === '4-wrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[6])
          }
        })
      }).timeout('20000')
      it('isFile: 1 - stdin: 1', () => {
        let packs = []
        makeBox('123_java', 'java', false).exec(
          pack => packs.push(pack),
          () => true,
          {stdin: 1, isFile: 1}
        )

        packs.forEach(pack => {
          if (pack.filename === '3-nowrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[5])
          } else if (pack.filename === '4-wrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[7])
          }
        })
      }).timeout('20000')
    })

    /** Test executing C++ Box */
    describe('C++', () => {
      it('isFile: 0 - stdin: 0', () => {
        makeBox('123_cpp', 'java', false).exec(
          pack => {
            if (pack.filename === '1-nowrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[0])
            } else if (pack.filename === '2-wrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[2])
            }
          },
          () => true,
          {stdin: 0, isFile: 0}
        )
      }).timeout('10000')
      it('isFile: 0 - stdin: 1', () => {
        makeBox('123_cpp', 'java', false).exec(
          pack => {
            if (pack.filename === '1-nowrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[1])
            } else if (pack.filename === '2-wrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[3])
            }
          },
          () => true,
          {stdin: 1, isFile: 0}
        )
      }).timeout('10000')
      it('isFile: 1 - stdin: 0', () => {
        let packs = []
        makeBox('123_cpp', 'java', false).exec(
          pack => packs.push(pack),
          () => true,
          {stdin: 0, isFile: 1}
        )

        packs.forEach(pack => {
          if (pack.filename === '3-nowrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[4])
          } else if (pack.filename === '4-wrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[6])
          }
        })
      }).timeout('20000')
      it('isFile: 1 - stdin: 1', () => {
        let packs = []
        makeBox('123_cpp', 'java', false).exec(
          pack => packs.push(pack),
          () => true,
          {stdin: 1, isFile: 1}
        )

        packs.forEach(pack => {
          if (pack.filename === '3-nowrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[5])
          } else if (pack.filename === '4-wrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[7])
          }
        })
      }).timeout('20000')
    })

    /** Test executing C Box */
    describe('C', () => {
      it('isFile: 0 - stdin: 0', () => {
        makeBox('123_c', 'java', false).exec(
          pack => {
            if (pack.filename === '1-nowrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[0])
            } else if (pack.filename === '2-wrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[2])
            }
          },
          () => true,
          {stdin: 0, isFile: 0}
        )
      }).timeout('10000')
      it('isFile: 0 - stdin: 1', () => {
        makeBox('123_c', 'java', false).exec(
          pack => {
            if (pack.filename === '1-nowrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[1])
            } else if (pack.filename === '2-wrap-line.txt') {
              expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[3])
            }
          },
          () => true,
          {stdin: 1, isFile: 0}
        )
      }).timeout('10000')
      it('isFile: 1 - stdin: 0', () => {
        let packs = []
        makeBox('123_c', 'java', false).exec(
          pack => packs.push(pack),
          () => true,
          {stdin: 0, isFile: 1}
        )

        packs.forEach(pack => {
          if (pack.filename === '3-nowrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[4])
          } else if (pack.filename === '4-wrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[6])
          }
        })
      }).timeout('20000')
      it('isFile: 1 - stdin: 1', () => {
        let packs = []
        makeBox('123_c', 'java', false).exec(
          pack => packs.push(pack),
          () => true,
          {stdin: 1, isFile: 1}
        )

        packs.forEach(pack => {
          if (pack.filename === '3-nowrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[5])
          } else if (pack.filename === '4-wrap-file.txt') {
            expect(utils.exclude(pack, ['size', 'filename'])).to.be.deep.equal(payload[7])
          }
        })
      }).timeout('20000')
    })
  })

  /** Test destroy() */
  describe('destroy()', () => {
    it('should delete Java Box Docker image', () => {
      const id = execSync(
        'docker images --filter=reference=i123_c --format "{{.ID}}"',
        {encoding: 'utf-8'}
      )

      expect(id).to.be.not.empty
    })
  })

  /** Test truncate() */
describe('truncate()', () => {
  it('should truncate Java Box', () => {
    return makeBox('123_java', 'java', false).truncate()
      .then(() => {
        expect(existsSync(`${prefix}_123_java`)).to.be.false
      })
  })
  it('should truncate C++ Box', () => {
    return makeBox('123_cpp', 'cpp', false).truncate()
      .then(() => {
        expect(existsSync(`${prefix}_123_cpp`)).to.be.false
      })
  })
  it('should truncate C Box', () => {
    return makeBox('123_c', 'cpp', false).truncate()
      .then(() => {
        expect(existsSync(`${prefix}_123_c`)).to.be.false
      })
  })
  it('should truncate cloned Box', () => {
    return makeBox('123_java-cloned', 'java', false).truncate()
      .then(() => {
        expect(existsSync(`${prefix}_123_java-cloned`)).to.be.false
      })
  })
})
})
