import {resolve} from "path";

export default () => ({
  /** Port config */
  server: {
    port: 3000,
    host: 'localhost',
    cli: process.env.MODE === 'dev'
  },

  /** API config */
  api: {
    prefix: '/api',
  },

  /** Sequelize config */
  sequelize: {
    dialect: 'sqlite',
    database: 'database',
    storage: resolve('storage/database/database.sqlite'),
    logging: false
  },

  /** Socket.io config */
  socket: {
    cors: {
      origin: "http://localhost:8080",
      methods: ["GET", "POST"]
    }
  },

  /** Auth config */
  auth: {
    secret: 'secret',
    model: 'User',
    /** jsonwebtoken config option */
    token: {
      /** Combination of: (<amount> <unit>). Space required */
      expiresIn: '7 days',
    }
  },

  /** Hash */
  hash: {
    round: 10
  },

  /** Box configuration */
  box: {
    maxBuffer: 1024 * 1024,
    timeout: 60 * 1000
  }
});
