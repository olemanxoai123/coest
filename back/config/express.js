import cli from "./../scripts/cli.js"
import config from "./config.js";

export default (app) => {
  const tools = cli(!config()['server']['cli']);

  tools.start('Boot plugins')
  //
  // //morgan
  // app.use(morgan('combined'));
  //
  // //body-parser
  // app.use(bodyParser.json());
  // app.use(bodyParser.urlencoded({ extended: false }));
  //
  // //cookie-parser
  // app.use(cookieParser());

  // //compression
  // app.use(compress());
  tools.stop('succeed');
}
