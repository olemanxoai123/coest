/** Node */
  import fse from "fs-extra"
import {exec, execSync, spawnSync} from "child_process"
import {appendFileSync, existsSync, readFileSync, mkdirSync, unlinkSync, createReadStream} from "fs"
import {resolve, extname, basename, parse, dirname} from "path"
import {createInterface} from "readline"
/** JSZip */
import JSZip from "jszip"
/** Glob */
import glob from "glob"
/** Lodash */
import _ from "lodash"
/** Config */
import config from "../config/config.js"
/** Utils */
import {is, log} from "../scripts/utils.js";

/**
 * Destructuring
 */
const {box} = config()
const {copySync, remove, removeSync} = fse
const {trim} = _

/** Make sure this option never exist */
delete box['cwd']

/**
 * Create new box or find box with id and type
 *
 * @param {string} id
 * @param {string} type
 * @param {boolean} logging
 */
function makeBox(id, type, logging = true) {
  return new Box(id, type, logging)
}

function wipe(id) {
  Box.wipe(id)
}

/**
 * Box for code compile and execute
 *
 * @class Box
 */
class Box {
  /**
   * Box ID
   *
   * @private
   * @memberOf Box
   */
  #id
  /**
   * Box path
   *
   * @private
   * @memberOf Box
   */
  #path
  /**
   * Box type
   *
   * @memberOf Box
   */
  #type
  /** Box logging flag */
  #logging


  /**
   * Create new Box instance
   *
   * @constructor
   * @memberOf Box
   *
   * @param {string} id Box ID
   * @param {string} type Type of box
   * @param {boolean} logging
   */
  constructor(id, type, logging = true) {
    this.#id = id
    this.#path = resolve(`storage/app/containers/_${id}`)
    this.#type = type
    this.#logging = logging

    /** Check if _id is exited */
    existsSync(this.#path)
      ? log(`Box _${id} is already existed`, this.#logging)
      : copySync(
        resolve(`utils/${type === 'java' ? 'java' : 'cpp'}/box`),
      resolve(`storage/app/containers/_${id}`))
  }

  /**
   * Get the path of Box
   *
   * @returns {string}
   */
  getPath() {
    return this.#path
  }

  /**
   * Build a file with chunk of U8IntArray
   *
   * @param {array|object} chunk
   * @param {string} ext File extension
   */
  craft(chunk, ext = 'zip') {
    try {
      const isBuffer = Buffer.isBuffer(chunk)
      Array.isArray(chunk) || isBuffer
        ? appendFileSync(`${this.#path}/archive.${ext}`, isBuffer ? chunk : Buffer.from(chunk))
        : appendFileSync(
        `${this.#path}/archive.${chunk['ext']}`,
        Buffer.from(chunk['buffer']) ?? new Buffer([])
        )
    } catch (error) {
      log(`[Box:${this.#id}] Craft failed\nError: ${error}`, this.#logging)
    }

    return this
  }

  /**
   * Clone Box for user
   *
   * @param {string} userId
   */
  clone(userId) {
    const newPath = `${this.#path}-${userId}`
    existsSync(newPath) || copySync(this.#path, newPath)

    return `${this.#id}-${userId}`
  }

  /**
   * Extract crafted resources
   *
   * @param {int} phase
   * @param {boolean} shouldDelete Delete the archive.zip
   */
  async extract(phase, shouldDelete = true) {
    const dir = phase === 1 ? 'archive' : 'src'
    existsSync(`${this.#path}/archive.zip`)
      ? await (
        async () => {
          try {
            // Create new JSZip instance
            let zip = new JSZip()
            // Load .zip file
            const archive = await zip.loadAsync(readFileSync(`${this.#path}/archive.zip`))
            // Iterate through all files in .zip file
            for (const name of Object.keys(archive['files'])) {
              // Prepare file buffer
              const buffer = await zip.file(name)?.async('nodebuffer')
              // Convert buffer to file
              is('empty', buffer) || (() => {
                // Create directory if not exist
                existsSync(`${this.#path}/${dir}/${dirname(name)}`) ||
                mkdirSync(`${this.#path}/${dir}/${dirname(name)}`)
                // Create file
                appendFileSync(`${this.#path}/${dir}/${name}`, buffer)
              })()
            }
            // Remove the file first
            existsSync(`${this.#path}/archive.zip`) && shouldDelete &&
            unlinkSync(`${this.#path}/archive.zip`)
          } catch (error) {
            log(`[Box:${this.#id}] Extract failed\nError: ${error}`, this.#logging)
          }
        }
      )()
      : (() => {
        copySync(`${this.#path}/archive.txt`, `${this.#path}/archive/archive.txt`)
        existsSync(`${this.#path}/archive.txt`) && shouldDelete &&
        unlinkSync(`${this.#path}/archive.txt`)
      })()
  }

  /**
   * Build script for Box
   *
   * @param {string} entry Entry filename
   */
  build(entry) {
    /** Build mandatory script */
    this.#buildScript(entry)
    /** Build box */
    this.#buildBox()

    return this
  }

  /**
   * Execute Box
   *
   * @memberOf Box
   *
   * @param {function} handler Handler for handle output data
   * @param {function} cb Callback on the end
   * @param {object} options Execution options
   */
  exec(handler, cb, options = {stdin: 0, isFile: 0}) {
    const files = this.#load()
    try {
      files
        .forEach((file, index) => !!options.isFile
          ? (() => {
            const line = trim(readFileSync(file, {encoding: 'utf-8'})).replace(/\r\n/g, '\n')
            /** Wrap exec user handler */
            this.#caseHandlerWrapper(line, {...options, handler, filename: basename(file)})
            /** Handle the end */
            index === files.length - 1 && cb()
          })()
          : (() => {
            createInterface({input: createReadStream(file), historySize: 0})
              /** Wrap exec user handler */
              .on(
                'line',
                (line) => (/^\n$/).test(line) || this.#caseHandlerWrapper(
                  line,
                  {...options, handler, filename: basename(file)}
                  )
              )
              /** Handle the end */
              .on('close', () => index === files.length - 1 && cb())
          })()
        )
    } catch (error) {
      log(`[Box:${this.#id}] Exec failed\nError: ${error}`, this.#logging)
    }

    return this
  }

  /**
   * Reload testcase
   */
  reload() {
    [
      `${this.#path}/archive`,
      `${this.#path}/archive.zip`,
      `${this.#path}/archive.txt`
    ]
      .forEach(path => existsSync(path) ?? removeSync(path))

    return this
  }

  /**
   * Delete src resources and Box's image
   *
   * @memberOf Box
   */
  reset() {
    existsSync(`${this.#path}/src`) && removeSync(`${this.#path}/src`)
    mkdirSync(`${this.#path}/src`)

    return this
  }

  /**
   * Delete Box's Docker image
   *
   * @memberOf Box
   */
  destroy() {
    exec(`docker rmi i${this.#id}`, box)

    return this
  }

  /**
   * Delete Box resource
   *
   * @memberOf Box
   */
  async truncate() {
    exec('docker system prune -f', box)
    exec('docker builder prune -a -f', box)
    existsSync(this.#path) && await remove(this.#path)
    log(`[Box:${this.#id}] Delete succeed`, this.#logging)

    return this
  }

  static wipe(id) {
    glob(
      resolve(`storage/app/containers/_${id}*`).toLowerCase(),
      {stat: false},
      (err, dirs) => dirs.forEach(dir => remove(dir))
    )
  }

  /**
   * Get list of archive files
   *
   * @memberOf Box
   *
   * @returns {array}
   */
  #load() {
    try {
      return glob.sync(`${this.#path}/archive/**/*.txt`, {nosort: true, nodir: true, stat: false})
    } catch (error) {
      log(`[Box:${this.#id}] Load failed\nError: ${error}`, this.#logging)
    }
  }

  /**
   * Build compile and execute script for Box instance
   *
   * @private
   * @memberOf Box
   *
   * @param {string} entry
   */
  #buildScript(entry) {
    const filename = `${entry}.${this.#type}`
    /** Build compile script */
    appendFileSync(`${this.#path}/src-compile.sh`, Template[`${this.#type === 'java' ? 'java' : 'cpp'}Compiler`](filename), {flag: 'w'})
    /** Build execute script */
    appendFileSync(`${this.#path}/src-execute.sh`, Template[`${this.#type === 'java' ? 'java' : 'cpp'}Executor`](filename), {flag: 'w'})
  }

  /**
   * Build the Box with Docker
   *
   * @returns {boolean}
   */
  #buildBox() {
    /** Box building process status */
    try {
      execSync(`docker build -q -t i${this.#id} --no-cache storage/app/containers/_${this.#id}`, box)
      log(`[Box:${this.#id}] Create succeed`, this.#logging)
      /** Make sure Box shouldn't be built multiple times */
    } catch (error) {
      log(`[Box:${this.#id}] Create failed\nError: ${error}`, this.#logging)
    }

    return this
  }

  /**
   * Escape escape character
   *
   * @memberOf Box
   * @static
   *
   * @param {string} value
   * @returns {string}
   */
  static cancelEscape(value) {
    return value.replace(/\\([\\rnt'"])/g, (m, g) => {
      if (g === 'n') return '\n';
      if (g === 'r') return '\r';
      if (g === 't') return '\t';
      if (g === '\\') return '\\';
      return g;
    })
  }

  /**
   * Exec handler wrapper
   *
   * @private
   * @memberOf Box
   *
   * @param {string} args
   * @param {string} expect
   * @param {string} plain
   * @param {object} options
   * @param {number} size
   * @param {string} filename
   */
  #execHandlerWrapper(args, expect, plain, options, size, filename) {
    log(
      `[Box:${this.#id}]RUN: docker run --rm -e ${!!options['stdin'] ? 'ARGS="" -i' : `ARGS="${args}" SEP="${options['mark']}"`} i${this.#id}`,
    this.#logging
    );
    // console.log(args)
    // console.log(expect)
    // console.log(plain)
    /** Execute Box container */
    const child = spawnSync(
      'docker',
      [
        'run', '--rm',
        ...!!options['stdin']
          ? ['-e', 'ARGS=""', '-e', 'SEP=""', '-i']
          : ['-e', `ARGS="${args}"`, '-e', `SEP="${options['mark']}"`,],
        `i${this.#id}`
      ],
      {
        encoding: 'utf-8', ...box,
        input: !!options['stdin'] ? args : ''
      },
    )

    /** Return Box output to handler */
    options['handler']({
      input: plain,
      ...child['stderr'] === ''//is('empty', child['output'][0])
        ? {output: child['stdout'], error: false}
        : {output: child['stderr'], error: true},
      expect: Box.cancelEscape(expect),
      size,
      filename
    })
  }

  /**
   * Pre-process testcase for exec
   *
   * @private
   * @memberOf Box
   *
   * @param {string} data
   * @param {object} options
   */
  #caseHandlerWrapper(data, options) {
    /** Determine main separator style */
    const sep = !!options['isFile']
      ? [.../"\n*"/.test(data) ? [/"\n*"/, '"'] : [/'\n*'/, "'"], /\n+/, '\n']
      : [.../"\s*"/.test(data) ? [/"\s*"/, '"'] : [/'\s*'/, "'"], /\s+/, ' ']
    const wrapped = (sep[0]).test(data)
    const input = data.trim().split(wrapped ? sep[0] : sep[2])
    const expect = input.pop()

    this.#execHandlerWrapper(
      /** Params */
      ...wrapped
        ? [
          /** Value */
          !!options['stdin']
            ? input.join('\n').substr(1)
            : input.join(sep[1]).substr(1),
          expect.slice(0, -1), // Expect
          input.join(`${sep[1]}${sep[3]}${sep[1]}`).concat(sep[1]), // Plain input
          {...options, mark: sep[1] === '"' ? '\\"' : sep[1]},
          data.length, // Actual data length
          options['filename'] // Testcase file name
        ]
        : [
          /** Value */
          !!options['stdin']
            ? input.join('\n')
            : input.join(sep[1]), // Change this to ' ' if what default of isFile is each word instead of whole file
          expect, // Expect
          input.join(sep[3]), // Plain input
          {...options, mark: sep[1] === '"' ? '\\"' : sep[1]},
          data.length + 1, // Actual data length with \n has been eliminated by readline
          options['filename'] // Testcase file name
        ],
    )
  }
}

/**
 * Script template
 *
 * @class Template
 */
class Template {
  /**
   * Get java compiler template
   *
   * @static
   *
   * @param {string} entry Entry file
   * @returns {string}
   */
  static javaCompiler(entry) {
    return (
      `#!/bin/bash\n` +
      `find -name "*.java" > sources.txt\n` +
      `javac @sources.txt\n` +
      `rm sources.txt`
    )
  }

  /**
   * Get javaExecutor
   *
   * @static
   * @memberOf Template
   *
   * @param {string} entry Entry file
   * @returns {string}
   */
  static javaExecutor(entry) {
    return (
      '#!/bin/bash\n' +
      'IFS="${SEP}" read -r -a array <<< "$ARGS"\n' +
      `java ${parse(entry).name}` + '"${array[@]}"'
    )
  }

  /**
   * Get cpp compiler template
   *
   * @static
   *
   * @param {string} entry Entry file
   * @returns {string}
   */
  static cppCompiler(entry) {
    return (
      `#!/bin/bash\n` +
      `${extname(entry) === '.c' ? 'gcc' : 'g++'} ${basename(entry)} -o ${parse(entry).name}.o`
    )
  }

  /**
   * Get cpp executor
   *
   * @static
   * @memberOf Template
   *
   * @param {string} entry Entry file
   * @returns {string}
   */
  static cppExecutor(entry) {
    return (
      '#!/bin/bash\n' +
      'IFS="${SEP}" read -r -a array <<< "$ARGS"\n' +
      `./${parse(entry).name}.o` + '"${array[@]}"'
    )
  }
}

export {
  makeBox, wipe
}
