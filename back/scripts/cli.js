import ora from 'ora';
import boxen from 'boxen';
import chalk from 'chalk';
import {logMemory} from "./utils.js";
import {writeSnapshot} from "heapdump";

/**
 * Return CLI instance
 *
 * @memberOf CLI
 *
 * @param {boolean} disable Determine if CLI should be disable
 * @returns {CLI}
 */
export default function (disable = false) {
  return disable
    ? new CLI({disable})
    : new CLI({ora, boxen, chalk, disable});
};

/**
 * Server terminal graphic
 *
 * @class CLI
 */
export class CLI {
  /**
   * CLI constructor
   *
   * @constructor
   * @memberOf CLI
   *
   * @param {object} tools
   */
  constructor(tools) {
    Object.keys(tools).forEach((keys) => this[keys] = tools[keys]);
    this['task'] = null;
  }


  /**
   * Create loading tasks
   *
   * @memberOf CLI
   *
   * @param {string|object} task Task name of task object
   */
  start(task) {
    /** Create new task and start */
    this['task'] = this['disable'] || this['ora'](
      typeof task === 'object'
        ? (() => {
          task['spinner'] = task['spinner'] === null || task['spinner'] === undefined
            ? {
              "interval": 80,
              "frames": [
                "⠋",
                "⠙",
                "⠹",
                "⠸",
                "⠼",
                "⠴",
                "⠦",
                "⠧",
                "⠇",
                "⠏"
              ]
            }
            : task['spinner']

          return task;
        })()
        : {
          text: task,
          spinner: {
            "interval": 80,
            "frames": [
              "⠋",
              "⠙",
              "⠹",
              "⠸",
              "⠼",
              "⠴",
              "⠦",
              "⠧",
              "⠇",
              "⠏"
            ]
          },
        }
    ).start();
  }

  /**
   * Stop and clear the task
   *
   * @memberOf CLI
   *
   * @param {string} method Stop method
   * @param {object} option stopAndPersist option
   */
  stop(method, option = {}) {
    /** Stop task */
    try {
      if (this['disable']) return
      method === 'stop'
        ? this['task']['stopAndPersist'](option)
        : this['task'][method]()
      /** Clear after stop */
      this['task'] = null;
    } catch (error) {
      console.log('Empty task')
    }
  }

  /**
   * Create line
   *
   * @memberOf CLI
   *
   * @param {int} length Length of line
   * @param {string} symbol Line symbol
   * @returns {string}
   */
  line(length, symbol = '-') {
    this['disable'] || console.log(new Array(length).fill(symbol).join(''));
  }

  /**
   * Render serve template
   *
   * @memberOf CLI
   *
   * @param {object} server
   */
  serve(server) {
    console.log(
      this['disable']
        ? `Listening on: http://${server.host}:${server.port}`
        : (() => {
          /** Local variable */
          const s = this['chalk'].hex('#2196F3').bold('»');
          let count = 0
          setInterval(() => {
            console.log(`${s} ${this['chalk'].yellowBright('Memory usage')}: ${logMemory()}`)
            const name = `${Date.now()}-${count++}`
            // writeSnapshot(`storage/logs/dump-${name}.heapsnapshot`, () => {
            //   console.log(`dump-${name}: ${logMemory()}`)
            // })
          }, 10000)

          /** Render */
          return this['boxen'](
            `${s} ${this['chalk'].yellowBright('Listening   ')}: http://${server.host}:${server.port}\n` +
            `${s} ${this['chalk'].yellowBright('Memory usage')}: ${logMemory()}\n` +
            `${s} ${this['chalk'].yellowBright('Link        ')}: https://google.com`,
            {
              padding: 1,
              margin: {left: 1},
              borderColor: 'green',
              borderStyle: 'round',
              align: 'left'
            }
          )
        })()
    )
  }

}
