/** Node */
import path from "path";
import bcrypt from "bcrypt";
import crypto from "crypto";
import {createHash} from "crypto";
import {memoryUsage} from "process";
import {appendFile} from "fs";
/** JWT */
import jwt from "jsonwebtoken";
/** Moment */
import moment from "moment";
/** Config */
import config from "../config/config.js";

const cf = config()

/**
 * Check the type of value
 *
 * @param {string} type Check type
 * @param {*} value Value
 * @returns {boolean}
 */
function is(type, value) {
  if (type === 'empty') {
    return value === null || value === undefined;
  } else if (type === 'array') {
    return Array.isArray(value);
  } else {
    return typeof value === type && !Array.isArray(value);
  }
}

/**
 * Craft a response
 *
 * @param {boolean} success
 * @param {*} data
 * @param {string} message
 * @returns {object}
 */
function craft(success, data = {}, message = '') {
  return {success, data, message}
}

/**
 * Sign data with secret key
 *
 * @param {object} data
 * @returns {string} JWT
 */
function signJWT(data) {
  return jwt.sign(data, cf.auth.secret, cf.auth.token)
}

/**
 * Exclude keys in object
 *
 * @param {object} object
 * @param {array} keys
 * @returns {object}
 */
function exclude(object, keys) {
  let out = Object.assign({}, object)
  keys.forEach((key) => delete out[key])
  return out
}

/**
 * Only key in object
 *
 * @param {object} object
 * @param {array} keys
 * @returns {object}
 */
function only(object, keys) {
  let out = {}
  keys.forEach((key) => out[key] = object[key])
  return out
}

/**
 * Hash a value with algorithm
 *
 * @param value
 * @param algorithm
 */
function hash(value, algorithm = 'sha256') {
  return createHash(algorithm)
    .update(value)
    .digest('base64')
    .replace(/\//g, '_')
}

/**
 * Hash a value with bcrypt and salt
 *
 * @param {string} value
 * @returns {*}
 */
function hashBcrypt(value) {
  return bcrypt.hashSync(
    value,
    bcrypt.genSaltSync(cf.hash.round)
  )
}

/**
 * Generate random string
 *
 * @param size
 * @returns {string}
 */
function randomStr(size) {
  return crypto
    .randomBytes(size)
    .toString('base64')
    .slice(0, size)
    .replace(/\//g, '_')
}

/**
 * Log used memory
 *
 * @returns {string}
 */
function logMemory() {
  const {heapUsed, rss, heapTotal} = memoryUsage()
  const cal = (value) => ((value / 1024 / 1024).toFixed(2))

  return `${cal(heapUsed)}/${cal(heapTotal)}MB (RSS: ${cal(rss)}MB)`;
}

/**
 * Logging content
 *
 * @param {*} content
 * @param {boolean} shouldConsoleLog
 */
function log(content, shouldConsoleLog = true) {
  shouldConsoleLog && console.log(content)
  const now = moment()
  appendFile(
    `./storage/logs/${now.format('MM-DD-YYYY')}.txt`,
    typeof content === 'string'
      ? `[${now.format('hh:mm:ss')}] ${content}\n`
      : `[${now.format('hh:mm:ss')}] ${JSON.stringify(content)}\n`,
    err => !!err && console.log(err)
  )
}

/**
 * Get token expires in time
 *
 * @returns {*}
 */
function tokenExpiresIn() {
  return cf.auth.token.expiresIn
    .split(' ')
    .map(((value, index) => index === 0 ? parseInt(value) : value))
}

export {
  is, craft, signJWT, exclude, only, hashBcrypt, randomStr, logMemory, log, hash, tokenExpiresIn
}
