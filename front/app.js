const
  express = require('express'),
  serveStatic = require('serve-static'),
  history = require('connect-history-api-fallback'),
  helmet = require('helmet'),
  http = require('http'),
  { resolve } = require('path'),
  { createProxyMiddleware } = require('http-proxy-middleware')

const app = express()
/** Apply helmet.js */
app.use(helmet({
  contentSecurityPolicy: {
    useDefaults: true,
    directives: {
      connectSrc: ["'self'", "ws://localhost:3000", "localhost:3000"]
    }
  }
}))
/** Add history fallback */
app.use(history())
/** Serve static */
app.use(serveStatic(resolve('./dist/spa')))
/** Proxy */
app.use(
  '/api',
  createProxyMiddleware({
    target: 'http://localhost:3000'
  })
)

const server = http.createServer(app)

server.listen(process.env.port ?? 8080, process.env.host)
