const state = {
  answer: []
}
const getters = {
  answerOf: (state) => (index) => {
    return state.answer[index]
  }
}
const mutations = {
  update (state, {i, n}) {
    state.answer[i] = n
  },
  clear (state) {
    state.answer = []
  },
  initiate (state, n) {
    state.answer = new Array(n).fill(0)
  }
}
const actions = {
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
