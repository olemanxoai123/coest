import {getField, updateField} from 'vuex-map-fields';
import {LocalStorage} from "quasar";

const state = {
  classic: !!LocalStorage.getItem('classic'),
  mini: !!LocalStorage.getItem('mini'),
  inst: !!LocalStorage.getItem('inst')
}
const getters = {
  getField
}
const mutations = {
  updateField
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
}
