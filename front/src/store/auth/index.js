import {getField, updateField} from 'vuex-map-fields';
import {LocalStorage} from "quasar";

const state = {
  lock: !!LocalStorage.getItem('lock'),
  token: LocalStorage.getItem('token'),
}
const getters = {
  getField
}
const mutations = {
  updateField,
  login(state, token) {
    state.lock = false
    state.token = token
  },
  logout(state) {
    state.lock = true
    state.token = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
}
