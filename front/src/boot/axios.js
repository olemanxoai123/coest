import Vue from 'vue'
import axios, {AxiosInstance} from 'axios'
import {LocalStorage} from "quasar";

const instance = axios.create({
  // baseURL: 'http://localhost:3000/api',
})

/**
 * Wrap axios
 *
 * @returns {AxiosInstance}
 */
function wrapper() {
  /** Add interceptor for Unauthorized response */
  instance.interceptors.response['handlers'].length === 0 && instance.interceptors.response.use(
    (res) => (res),
    (err) => {
      if (err.response && err.response.status === 401) {
        this.$logout()

        return Promise.reject(null)
      }

      return Promise.reject(err)
    }
  )
  /** Automatic grab the access token in Vuex */
  instance.defaults.headers.common['Authorization'] =
    this.$store.state.auth.token === null || this.$store.state.auth.token === undefined
      ? LocalStorage.getItem('access')
      : this.$store.state.auth.token
  return {
    get: (path, ...args) => instance.get(`/api${path}`, ...args),
    post: (path, ...args) => instance.post(`/api${path}`, ...args),
    patch: (path, ...args) => instance.patch(`/api${path}`, ...args),
    put: (path, ...args) => instance.put(`/api${path}`, ...args),
    delete: (path, ...args) => instance.delete(`/api${path}`, ...args)
  }
}

Vue.prototype.$axios = wrapper
//
// const isPrivate = (method) => (method === 'post' || method === 'put' || method === 'patch')
//
// Vue.prototype.$axios = function (method, url, payload, auto = true) {
//   method = method.toString().trim().toLowerCase();
//   return auto
//     ? service[method](url, isPrivate() ? payload : {params: payload})
//       .then(res => this['onSuccess'] ? this['onSuccess'](res) : null)
//       .catch(err => this['onError'] ? this['onError'](err) : null)
//       .then(() => this['onFinal'] ? this['onFinal'] : null)
//     : service[method](url, isPrivate() ? payload : {params: payload});
// }
