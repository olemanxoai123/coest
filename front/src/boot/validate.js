import Vue from "vue"
import {isEmail} from "validator";

Vue.prototype.$rule = function (field, size, empty) {
  return size === null || size === undefined
    ? []
    : [
      field === 'email'
        ? v => isEmail(`${v}`) || 'Invalid email'
        : v => v && v.length > (empty === true ? -1 : 0) || `${field} is required`,
      v => v.length <= size || `Maximum character is ${size}`
    ]
}
