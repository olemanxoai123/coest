import Vue from "vue"
import {Notify} from 'quasar'
import {coest} from "boot/coest";

Vue.prototype.$notify = {
  pos(message = 'An error has occurred', position = 'top-right') {
    Notify.create({
      color: coest.qCDone,
      icon: 'done',
      badgeColor: 'black',
      badgeTextColor: 'white',
      textColor: 'white',
      position: position,
      classes: 'coest-font-dosis-only text-weight-bolder',
      message: message,
      actions: [{
        icon: 'close', color: 'white', handler: () => ({}), classes: 'coest-font-dosis-only text-weight-bolder'
      }],
      timeout: 2000
    })
  },
  neg(message, position = 'top-right') {
    Notify.create({
      icon: 'error',
      textColor: 'white',
      badgeColor: 'black',
      badgeTextColor: 'white',
      message: message,
      color: coest.qCNone,
      position: position,
      classes: 'coest-font-dosis-only text-weight-bolder',
      actions: [{
        icon: 'close', color: 'white', handler: () => ({}), classes: 'coest-font-dosis-only text-weight-bolder'
      }],
      timeout: 2000
    })
  }
}
