import Vue from "vue"
import {LocalStorage} from "quasar";

Vue.prototype.$login = function (token) {
  this.$store.commit('auth/login', token)
  LocalStorage.set('access', token)
  LocalStorage.set('lock', false)
}

Vue.prototype.$logout = function () {
  this.$store.commit('auth/logout')
  LocalStorage.remove('access')
  LocalStorage.set('lock', true)
}


