import Vue from "vue";
import {io, Socket} from "socket.io-client";
import {LocalStorage} from "quasar";

/**
 * Socket.io client wrapper
 *
 * @param {string} url
 * @param {function} callback
 * @returns {Socket}
 */
function wrapper(url, callback = null) {
  if (this.socket === null || this.socket === undefined) {
    const socket = io(
      'http://localhost:3000',
      {
        extraHeaders: {
          authorization: this.$store.state.auth.token === null || this.$store.state.auth.token === undefined
            ? LocalStorage.getItem('access')
            : this.$store.state.auth.token
        },
        forceNew: false,
        reconnection: false
      }
    );

    return socket.on('connect', (client) => socket
      .emit('init', url)
      .on('status', (success) => success && (() => {
        // /** Socket has 10 min session before timeout (global timeout) */
        // setTimeout(() => socket.disconnected || socket.emit('drop'), 600000)

        // /** Socket will disconnect when inactive for 20s (local timeout) */
        // let localTimeout = setTimeout(
        //   () => socket.disconnected || socket.emit('drop'),
        //   30000
        // )
        socket.prependAny(() => {
          // clearTimeout(localTimeout)
          // localTimeout = setTimeout(
          //   () => socket.disconnected || socket.emit('drop'),
          //   30000
          // )
        })

        this.socket = socket

        /** Append event and listen */
        typeof callback === 'function' && callback(socket, client)
      })())
    )
      .on('connect_error', (error) => error.message === 'Unauthorized' && this.$logout())
  } else {
    callback()

    return this.socket
  }
}

/**
 * Loop all events set invoke socket.off on each
 *
 * @param {array} events
 */
function offWrapper(events) {
  events.forEach((value) => this.socket?.off(value))
}

Vue.prototype.$socket = wrapper
Vue.prototype.$sOff = offWrapper
