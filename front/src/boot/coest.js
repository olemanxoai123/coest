import Vue from 'vue';

const coest = {
  /** Main color */
  lightTextColor: '#697a89',
  textColor: '#091e42',
  qColor: 'purple-9',
  qCProc: 'orange-9',
  qCDone: 'green-13',
  qCNone: 'red-7',
  qCInfo: 'cyan-6',
  qBackground: 'blue-grey-4',
  primaryColor: '#691297',
  background: '#F8FAFC',

  /** WYSIWYG config */
  editor: [
    ['left', 'center', 'right', 'justify'],
    ['bold', 'italic', 'strike', 'underline', 'subscript', 'superscript'],
    ['hr', 'link', 'fullscreen', 'viewsource'],
    [
      {
        label: 'Formatting',
        icon: 'text_format',
        list: 'no-icons',
        options: [
          'p',
          'h1',
          'h2',
          'h3',
          'h4',
          'h5',
          'h6',
          'code'
        ]
      },
      {
        label: 'Font Size',
        icon: 'format_size',
        fixedLabel: true,
        fixedIcon: true,
        list: 'no-icons',
        options: [
          'size-1',
          'size-2',
          'size-3',
          'size-4',
          'size-5',
          'size-6',
          'size-7'
        ]
      },
      'removeFormat'
    ],
    ['quote', 'unordered', 'ordered', 'outdent', 'indent'],
    ['undo', 'redo'],
  ],

  /** Page width */
  pageWidth: 600,

  /** Scrollbar style */
  scrollbar: {
    bar: {
      right: '2px',
      borderRadius: '9px',
      backgroundColor: '#CE93D8',
      width: '9px',
      widthValue: 9,
      opacity: 0.2
    },
    thumb: {
      right: '4px',
      borderRadius: '5px',
      backgroundColor: '#691297',
      width: '5px',
      widthValue: 5,
      opacity: 0.75
    }
  },

  /** Structure */
  structure: {
    auth: [['email'], ['password'], ['name'], ['id', 'class'], ['expertise']],
    profile: ['id', 'email', 'class', 'expertise', 'faculty', 'role'],
    add: [['code'], ['name'], ['deadline', 'password'], ['note']],
  },

  /** Text field to be shown */
  field: {
    add: {
      code: {
        field: 'Code',
        icon: 'fingerprint',
        max: 21
      },
      name: {
        field: 'Name',
        icon: 'badge',
        max: 50
      },
      password: {
        field: 'Password',
        icon: 'lock',
        max: 20,
        empty: true
      },
      deadline: {
        field: 'Deadline',
        icon: 'event',
        max: 18
      },
      note: {
        field: 'Description',
        icon: 'description'
      }
    },
    profile: {
      email: {
        field: 'Email',
        icon: 'email',
        color: 'red-5',
        max: 40
      },
      password: {
        field: 'Password',
        icon: 'lock',
        max: 100
      },
      name: {
        field: 'Name',
        icon: 'perm_identity',
        max: 40
      },
      id: {
        field: 'ID',
        icon: 'fingerprint',
        color: 'cyan-12',
        max: 8
      },
      class: {
        field: 'Class',
        icon: 'room',
        color: 'brown-7',
        max: 10
      },
      faculty: {
        field: 'Faculty',
        icon: 'school',
        color: 'blue-9',
      },
      expertise: {
        field: 'Expertise',
        icon: 'history_edu',
        color: 'amber-14',
        max: 40
      },
      role: {
        field: 'Role',
        icon: 'security',
        color: 'green-6',
        max: 12
      }
    },
  },

  box: {
    url: 'http://localhost:8080/Box',
    perPage: 20
  },


  /** Profile to be shown in AuthPanel and Profile component */
  upload: {
    type: {
      value: ['java', 'c++', 'c'],
      img: {
        'java': 'images/java.png',
        'c++': 'images/cpp.svg',
        'c': 'images/c.png'
      }
    },
  },

  options: {
    stdin: [
      {
        label: 'Arguments',
        value: 0
      },
      {
        label: 'Stdin',
        value: 1
      }
    ],
    isFile: [
      {
        label: 'Line',
        value: 0
      },
      {
        label: 'File',
        value: 1
      }
    ]
  },

  download: {
    type: {
      options: [
        {label: 'HTML (Hypertext Markup Language)', value: 'html'},
        {label: 'CSV (Comma-Separated Values)', value: 'csv'},
        {label: 'XLSX (Excel Microsoft Office Open XML Format Spreadsheet)', value: 'xlsx'}
      ],
      img: {
        'html': 'images/html.png',
        'csv': 'images/csv.png',
        'xlsx': 'images/xlsx.png'
      }
    }
  },

  export: {
    detail: {
      html: {
        headers: [
          {label: 'Input', width: 200, value: 'input', asClass: false},
          {label: 'Output', width: 300, value: 'output', asClass: false},
          {label: 'Expected', width: 300, value: 'expected', asClass: false},
          {label: 'Final', width: 100, value: 'final', asClass: true},
        ],
        base: '<!DOCTYPE html> <html> <head> <style> table, th, td { border: 1px solid black; } table { table-layout: fixed } td { word-wrap: break-word; } tr>.true:last-child { background-color: greenyellow; } tr>.false:last-child { background-color: red } </style> </head> <body> <coest-tables> </body></html>',
        tag: '<coest-tables>',
        key: 'result'
      },
      csv: {
        headers: ['input', 'output', 'expected', 'final'],
        key: 'result'
      },
      xlsx: {
        headers: ['input', 'output', 'expected', 'final'],
        key: 'result'
      },
    },
    summary: {
      html: {
        base: '<!DOCTYPE html> <html> <head> <style> table, th, td { border: 1px solid black; } table { table-layout: fixed } td { word-wrap: break-word; } </style> </head> <body> <coest-tables> </body></html>',
        tag: '<coest-tables>',
        key: 'result'
      },
      csv: {
        key: 'result'
      },
      xlsx: {
        key: 'result'
      },
    }
  }
}

Vue.prototype.coest = coest

export {coest}
