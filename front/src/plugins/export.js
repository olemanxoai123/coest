import XLSX from "xlsx"
import saveAs from "file-saver"

/**
 * Generate HTML table for provided data
 *
 * @param {*} payload
 * @param {string} key
 * @param {array} headers
 * @returns {string}
 */
function genHTMLTables(payload, key, headers = [{label: '', width: 0, value: '', asClass: false}]) {
  const tablesData = Array.isArray(payload) ? payload : [payload]
  const tables = tablesData.map(tableData => {
    let table = []
    /** Create header */
    table.push(headers
      .map(value => `<th style="width: ${value['width'] ?? 100}px">${value['label'] ?? value}</th>`)
      .join(' '))

    /** Create rows of data */
    tableData[key].forEach(child => table.push(
      headers
        .map(header =>
          `<td class="${header['asClass'] ? child[header['value'] ?? header] : ''}">` +
          `${header['asClass'] ? '' : child[header['value'] ?? header]}` +
          `</td>`
            .replace('\n', '<br>'))
        .join(' '))
    )

    /** Create whole table */
    return `<h2>${tableData['name']}</h2> <table> <tr> ${table.join('</tr> <tr>')} </tr> </table>`
  })

  return tables.join(' ')
}

/**
 * Return export HTML function
 *
 * @param {object} payload
 * @param {string} name
 * @param {object} options
 * @returns {function}
 */
function exportHTML(payload, name, options) {
  // Generate html tables
  const htmlTables = genHTMLTables(payload, options['key'], options['headers'])
  // Generate Blob for FileSaver
  const blob = new Blob(
    [options['base'].replace(options['tag'], htmlTables)],
    {type: 'text/octet-stream;charset=utf-8'}
  )
  // Return export function
  return () => saveAs(blob, `${name.replace(/\.zip$/, '')}.html`,)
}

/**
 * Transform data to csv format for preprocess
 *
 * @param {array|object} payload
 * @param {string} key
 * @param {array} headers
 * @param {function} callback
 * @returns {*}
 */
function genCSVTables(payload, key, headers, callback) {
  const tablesData = Array.isArray(payload) ? payload : [Object.assign({}, payload)]
  return tablesData.map(table => {
    table[key].forEach(row =>
      headers.forEach(header =>
        row[header] = typeof row[header] === 'string'
          ? row[header].replace(/(?<![\\])\n/g, '\\n')
          : row[header]))

    return callback(table)
  })
}

/**
 * Return export csv function
 *
 * @param {array|object} payload
 * @param {string} name
 * @param {object} options
 * @returns {function}
 */
function exportCSV(payload, name, options) {
  // Preprocess csv table
  const csv = genCSVTables(payload, options['key'], options['headers'], table => {
    const sheet = XLSX.utils.json_to_sheet(table[options['key']], {header: options['headers']})

    return `${table['name']}\n${XLSX.utils.sheet_to_csv(sheet)}`
  })
  // Create blob for FileSaver
  const blob = new Blob(
    [csv.join('\n')],
    {type: 'text/octet-stream;charset=utf-8'}
  )
  // Return export function
  return () => saveAs(blob, `${name.replace(/\.zip$/, '')}.csv`,)
}

function exportXLSX(payload, name, options) {
  // Create workbook
  const wb = XLSX.utils.book_new()
  // Preprocess csv table
  genCSVTables(payload, options['key'], options['headers'], table => {
    const sheet = XLSX.utils.json_to_sheet(table[options['key']], {header: options['headers']})
    XLSX.utils.book_append_sheet(wb, sheet, table['name'])
  })

  return () => XLSX.writeFile(wb, `${name.replace(/\.zip$/, '')}.xlsx`)
}


export {genHTMLTables, exportHTML, exportCSV, exportXLSX}
