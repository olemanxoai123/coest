
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/upload', component: () => import('pages/Upload.vue') },
      { path: '/manage', component: () => import('pages/Manage.vue') },
      { path: '/box', component: () => import('pages/Search.vue') },
      { path: '/box/:code', component: () => import('pages/Box.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
